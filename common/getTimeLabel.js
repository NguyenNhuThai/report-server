
var currentTime = function() {

    var month = new Array();
    month[0] = "Jan";
    month[1] = "Feb";
    month[2] = "Mar";
    month[3] = "Apr";
    month[4] = "May";
    month[5] = "Jun";
    month[6] = "Jul";
    month[7] = "Aug";
    month[8] = "Sep";
    month[9] = "Oct";
    month[10] = "Nov";
    month[11] = "Dec";

    var fullMonth = new Array();
    fullMonth[0] = "January";
    fullMonth[1] = "February";
    fullMonth[2] = "March";
    fullMonth[3] = "April";
    fullMonth[4] = "May";
    fullMonth[5] = "June";
    fullMonth[6] = "July";
    fullMonth[7] = "August";
    fullMonth[8] = "September";
    fullMonth[9] = "October";
    fullMonth[10] = "November";
    fullMonth[11] = "December";

     var dayOfMonth = new Array();
    dayOfMonth[0] = 31;
    dayOfMonth[1] = 28;
    dayOfMonth[2] = 31;
    dayOfMonth[3] = 30;
    dayOfMonth[4] = 31;
    dayOfMonth[5] = 30;
    dayOfMonth[6] = 31;
    dayOfMonth[7] = 31;
    dayOfMonth[8] = 30;
    dayOfMonth[9] = 31;
    dayOfMonth[10] = 30;
    dayOfMonth[11] = 31;

    var day = new Array();
    day[0] = "Sun";
    day[1] = "Mon";
    day[2] = "Tue";
    day[3] = "Wed";
    day[4] = "Thu";
    day[5] = "Fri";
    day[6] = "Sat";

    var curr = new Date; 
    var first = curr.getDate() - curr.getDay() + 1;
    var firstDay = new Date(curr.setDate(first))
    var lastDay = addDays(firstDay,6);
    var fistDayLabel = firstDay.getDate();
    var lastDayLabel = lastDay.getDate();
    var lastMonth = lastDay.getMonth() + 1;
    if (lastMonth < 10) { lastMonth = '0' + lastMonth; }
    var firstMonth = firstDay.getMonth() + 1;
    if (firstMonth < 10) { firstMonth = '0' + firstMonth; }
    var currentTimeLabel = "";
  
    
    if(firstDay.getFullYear() != lastDay.getFullYear()){
        if (firstDay.getDate() < 10){
             currentTimeLabel = "0"+firstDay.getDate() + "/" + firstMonth + "/" + firstDay.getFullYear();
        }else{
             currentTimeLabel = firstDay.getDate() + "/" + firstMonth + "/" + firstDay.getFullYear();
        }
        if (lastDay.getDate() < 10){
            currentTimeLabel = currentTimeLabel + " - 0" + lastDay.getDate() + "/" + lastMonth + "/" +lastDay.getFullYear();
        }else{
            currentTimeLabel = currentTimeLabel + " - " + lastDay.getDate() + "/" + lastMonth + "/" +lastDay.getFullYear();
        }
    }else{

       if (firstDay.getDate() < 10){
             currentTimeLabel = "0"+firstDay.getDate() + "/" + firstMonth;
        }else{
             currentTimeLabel = firstDay.getDate() + "/" + firstMonth;
        }
        if (lastDay.getDate() < 10){
            currentTimeLabel = currentTimeLabel + " - 0" + lastDay.getDate() + "/" + lastMonth + "/" +lastDay.getFullYear();
        }else{
            currentTimeLabel = currentTimeLabel + " - " + lastDay.getDate() + "/" + lastMonth + "/" +lastDay.getFullYear();
        }
    }



    var firstDayPreWeek = minusDays(firstDay,7);
    var lastDayPreWeek = minusDays(lastDay,7);
    var firstDayNextWeek = addDays(firstDay,7);
    var lastDayNextWeek = addDays(lastDay,7);
    var fistDayPreLabel = firstDayPreWeek.getDate();
    var lastDayPretLabel = lastDayPreWeek.getDate();
    var fistDayNextLabel = firstDayNextWeek.getDate();
    var lastDayNextLabel = lastDayNextWeek.getDate();
    var lastMonthPreWeek = lastDayPreWeek.getMonth() + 1;
    if (lastMonthPreWeek < 10) { lastMonthPreWeek = '0' + lastMonthPreWeek; }

    var firstMonthPreWeek = firstDayPreWeek.getMonth() + 1;
    if (firstMonthPreWeek < 10) { firstMonthPreWeek = '0' + firstMonthPreWeek; }

    var lastMonthNextWeek = lastDayNextWeek.getMonth() + 1;
    if (lastMonthNextWeek < 10) { lastMonthNextWeek = '0' + lastMonthNextWeek; }

    var firstMonthNextWeek = firstDayNextWeek.getMonth() + 1;
    if (firstMonthNextWeek < 10) { firstMonthNextWeek = '0' + firstMonthNextWeek; }
    var preTimeLabel = "";
    if(firstDayPreWeek.getFullYear() != firstDayPreWeek.getFullYear()){
         if (firstDayPreWeek.getDate() < 10){
             preTimeLabel = "0"+firstDayPreWeek.getDate() + "/" + firstMonthPreWeek + "/" + firstDayPreWeek.getFullYear();
        }else{
             preTimeLabel = firstDayPreWeek.getDate() + "/" + firstMonthPreWeek + "/" + firstDayPreWeek.getFullYear();
        }
        if (lastDayPreWeek.getDate() < 10){
            preTimeLabel = preTimeLabel + " - 0" + lastDayPreWeek.getDate() + "/" + lastMonthPreWeek + "/" +lastDayPreWeek.getFullYear();
        }else{
            preTimeLabel = preTimeLabel + " - " + lastDayPreWeek.getDate() + "/" + lastMonthPreWeek + "/" +lastDayPreWeek.getFullYear();
        }
    }else{
         if (firstDayPreWeek.getDate() < 10){
             preTimeLabel = "0"+firstDayPreWeek.getDate() + "/" + firstMonthPreWeek;
        }else{
             preTimeLabel = firstDayPreWeek.getDate() + "/" + firstMonthPreWeek;
        }
        if (lastDayPreWeek.getDate() < 10){
            preTimeLabel = preTimeLabel + " - 0" + lastDayPreWeek.getDate() + "/" + lastMonthPreWeek + "/" +lastDayPreWeek.getFullYear();
        }else{
            preTimeLabel = preTimeLabel + " - " + lastDayPreWeek.getDate() + "/" + lastMonthPreWeek + "/" +lastDayPreWeek.getFullYear();
        }
    }
    var nextTimeLabel = "";
    if(firstDayNextWeek.getFullYear() != lastDayNextWeek.getFullYear()){
        if (firstDayNextWeek.getDate() < 10){
             nextTimeLabel = "0"+firstDayNextWeek.getDate() + "/" + firstMonthNextWeek + "/" + firstDayNextWeek.getFullYear();
        }else{
             nextTimeLabel = firstDayNextWeek.getDate() + "/" + firstMonthNextWeek + "/" + firstDayNextWeek.getFullYear();
        }
        if (lastDayPreWeek.getDate() < 10){
            nextTimeLabel = nextTimeLabel + " - 0" + lastDayNextWeek.getDate() + "/" + lastMonthNextWeek + "/" +lastDayNextWeek.getFullYear();
        }else{
            nextTimeLabel = nextTimeLabel + " - " + lastDayNextWeek.getDate() + "/" + lastMonthNextWeek + "/" +lastDayNextWeek.getFullYear();
        }
    }else{
           if (firstDayNextWeek.getDate() < 10){
             nextTimeLabel = "0"+firstDayNextWeek.getDate() + "/" + firstMonthNextWeek ;
        }else{
             nextTimeLabel = firstDayNextWeek.getDate() + "/" + firstMonthNextWeek ;
        }
        if (lastDayNextWeek.getDate() < 10){
            nextTimeLabel = nextTimeLabel + " - 0" + lastDayNextWeek.getDate() + "/" + lastMonthNextWeek + "/" +lastDayNextWeek.getFullYear();
        }else{
            nextTimeLabel = nextTimeLabel + " - " + lastDayNextWeek.getDate() + "/" + lastMonthNextWeek + "/" +lastDayNextWeek.getFullYear();
        }
    }

    var textFromDate =  firstDay.getDate() + " " + month[firstDay.getMonth()] + " " + firstDay.getFullYear()
    var textToDate =  lastDay.getDate() + " " + month[lastDay.getMonth()] + " " + lastDay.getFullYear()


    var curr = new Date; 
    var currentDay = curr.getDay();
    var currentDate = curr.getDate();



    this.getTextFromDate = function(){
        return textFromDate;
    }
    this.getTextToDate = function(){
        return textToDate;
    }


    this.getPreTimeLabel = function(){
        return preTimeLabel;
    }

    this.getNextTimeLabel = function(){
        return nextTimeLabel;
    }

    this.getFistDayPreLabel = function(){
        return fistDayPreLabel;
    }

    this.getLastDayPretLabel = function(){
        return lastDayPretLabel;
    }

    this.getFistDayNextLabel = function(){
        return fistDayNextLabel;
    }

    this.getDastDayNextLabel = function(){
        return lastDayNextLabel;
    }


    this.getCurrentTimeLabel = function(){
        return currentTimeLabel;
    }
    this.getFirstDayLabel = function(){
        return fistDayLabel;
    }
    this.getLastDayLabel = function(){
        return lastDayLabel;
    }    
    this.getCurrentDayLabel = function(){
        return currentDate;
    }   
    this.getCurrentDateLabel = function(){
        day = curr.getDate();
        month = curr.getMonth() + 1;
        if(day<10){day="0"+day};
        if(month<10){month="0"+month};
        return day+"/"+month+"/"+curr.getFullYear();
    }  
    this.getTimeLabel = function(){
        day = curr.getDate();
        month = curr.getMonth() + 1;
        hours = curr.getHours();
        minutes =curr.getMinutes();
        if(day<10){day="0"+day};
        if(month<10){month="0"+month};
        if(hours<10){hours="0"+hours};
        if(minutes<10){minutes="0"+minutes};
        return day+month+curr.getFullYear()+hours+minutes;
    }   
    this.getMonth = function(){
        day = curr.getDate();
        month = curr.getMonth() + 1;
        return month;
    }   
    this.getMonthLabel = function(){
        console.log(curr.getMonth()+" current Month")
        return fullMonth[curr.getMonth()];
    }   
    this.getMonthTimeStamp = function(){
        time1 = "01/"+(curr.getMonth()+1)+"/"+curr.getFullYear();
        time2 = dayOfMonth[curr.getMonth()]+"/"+(curr.getMonth()+1)+"/"+curr.getFullYear();    
        var date2 =new Date(time2.split('/')[2],time2.split('/')[1]-1,time2.split('/')[0] )
        var date1 =new Date(time1.split('/')[2],time1.split('/')[1]-1,time1.split('/')[0] )
        return date1.getTime()+";"+date2.getTime();
    }   
    this.getPreMonthLabel = function(){
        if(curr.getMonth() == 0){
            return fullMonth[11];
        }
        return fullMonth[curr.getMonth()-1];
    }
    this.getPreMonthTimeStamp = function(){
        if(curr.getMonth() == 0){
            time1 = "01/"+"12/"+curr.getFullYear()-1;
            time2 = "31/"+"12/"+curr.getFullYear()-1;  
        }else{
            time1 = "01/"+(curr.getMonth())+"/"+curr.getFullYear();
            time2 = dayOfMonth[curr.getMonth()-1]+"/"+(curr.getMonth())+"/"+curr.getFullYear();    
        }
        var date2 =new Date(time2.split('/')[2],time2.split('/')[1]-1,time2.split('/')[0] )
        var date1 =new Date(time1.split('/')[2],time1.split('/')[1]-1,time1.split('/')[0] )
        return date1.getTime()+";"+date2.getTime();
    }
    this.getNextMonthLabel = function(){
        if(curr.getMonth() == 11){
            return fullMonth[0];
        }
        return fullMonth[curr.getMonth()+1];
    }   
    this.getNextMonthTimeStamp = function(){
        if(curr.getMonth() == 11){
            time1 = "01/"+"01/"+curr.getFullYear()+1;
            time2 = "31/"+"01/"+curr.getFullYear()+1;  
        }else{
            time1 = "01/"+(curr.getMonth()+2)+"/"+curr.getFullYear();
            time2 = dayOfMonth[curr.getMonth()+1]+"/"+(curr.getMonth()+2)+"/"+curr.getFullYear();    
        }
        var date2 =new Date(time2.split('/')[2],time2.split('/')[1]-1,time2.split('/')[0] )
        var date1 =new Date(time1.split('/')[2],time1.split('/')[1]-1,time1.split('/')[0] )
        return date1.getTime()+";"+date2.getTime();
    }
    this.getYear = function(){
        day = curr.getDate();
        year = curr.getFullYear()
        return year;
    }  

    this.getYearTimeStamp= function(){
        year = curr.getFullYear();
        time1 = "01/01/"+year
        time2 = "31/12/"+year
        var date2 =new Date(time2.split('/')[2],time2.split('/')[1]-1,time2.split('/')[0] )
        var date1 =new Date(time1.split('/')[2],time1.split('/')[1]-1,time1.split('/')[0] )
        return date1.getTime()+";"+date2.getTime();
    }  
    this.getPreYear = function(){
        day = curr.getDate();
        year = curr.getFullYear()
        return year-1;
    }  

    this.getPreYearTimeStamp = function(){
        year = curr.getFullYear()-1;
        time1 = "01/01/"+year
        time2 = "31/12/"+year
        var date2 =new Date(time2.split('/')[2],time2.split('/')[1]-1,time2.split('/')[0] )
        var date1 =new Date(time1.split('/')[2],time1.split('/')[1]-1,time1.split('/')[0] )
        return date1.getTime()+";"+date2.getTime();
    }  

    this.getNextYear = function(){
        year = curr.getFullYear()
        return year+1;
    }    

    this.getNextYearTimeStamp = function(){
        year = curr.getFullYear()+1;
        time1 = "01/01/"+year
        time2 = "31/12/"+year
        var date2 =new Date(time2.split('/')[2],time2.split('/')[1]-1,time2.split('/')[0] )
        var date1 =new Date(time1.split('/')[2],time1.split('/')[1]-1,time1.split('/')[0] )
        return date1.getTime()+";"+date2.getTime();

    }    

    
};
module.exports = new currentTime();


function addDays(theDate, days) {
    return new Date(theDate.getTime() + days*24*60*60*1000);
}
function minusDays(theDate, days) {
    return new Date(theDate.getTime() - days*24*60*60*1000);
}
