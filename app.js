var express = require("express");
var app = express();
var port = 2706;
app.use(express.static(__dirname +"/controller"))
var apiController = require("./controller/apiController.js");
var kbrController = require("./controller/kbriotController.js");
var vennpointController = require("./controller/vennpointController.js");
var dccController = require("./controller/dccController.js");

apiController(app)
vennpointController(app)
kbrController(app)
dccController(app)

app.set("view engine","ejs")
app.listen(port,function(){
    console.log("report server is listening on PORT ", port)
})



