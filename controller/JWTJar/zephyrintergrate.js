var getTime = require('../../common/getTimeLabel.js');
var config = require('./kbrjwtconfig.json');
var build_key = getTime.getTimeLabel();
var querystring = require('querystring');
var http 		= 	require('https');
var sys = require('sys');
//var exec = require('child_process').exec;
var exec = require('child-process-promise').exec;
var currentJWT
function puts(error, stdout, stderr) { 
    currentJWT = stdout.substring(stdout.indexOf("JWT KEY:") +8,stdout.length);
}
var urlCreateCycle = 'https://prod-api.zephyr4jiracloud.com/connect/public/rest/api/1.0/cycle?expand=&clonedCycleId='; 
var urlCreateExecution = 'https://prod-api.zephyr4jiracloud.com/connect/public/rest/api/1.0/execution';
var urlUpdateExecution = 'https://prod-api.zephyr4jiracloud.com/connect/public/rest/api/1.0/execution/{executionID}';

var zephyrintergrate = function() { 
    this.createNewCycle = function(){
       // exec('java -jar getJWTCreateCycle.jar', function callback(error, stdout, stderr){
       return exec('java -jar getJWTCreateCycle.jar').then(function(rs){
            var stdout = rs.stdout
            text = stdout.substring(stdout.indexOf("JWT KEY:") +8,stdout.length -2)
            console.log("Cycle JWT key: " + stdout.substring(stdout.indexOf("JWT KEY:") +8,stdout.length -2));
           // console.log("text         :" , text)
            var cycleName = config.NAME_CYCLE_FREFIX+getTime.getTimeLabel();
            var description = "This cycle create by automation at " +  getTime.getCurrentTimeLabel();
            var projectID = config.PROJECT_ID;
            var versionID = config.VERSION_ID;
            var zapiAccessKey = config.ZAPI_ACCESS_KEY;
            return new Promise((resolve, reject) => {
                //body
                var post_data = {
                    "name": cycleName,
                    "description": description,
                    "projectId": projectID,
                    "versionId": versionID
                };
                post_data =  JSON.stringify(post_data)
                // header
                var post_options = {
                    host: 'prod-api.zephyr4jiracloud.com',
                    port: '443',
                    path: '/connect/public/rest/api/1.0/cycle?expand=&clonedCycleId=',
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': text,
                        'zapiAccessKey': zapiAccessKey,
                        'Content-Length': Buffer.byteLength(post_data)
                    }
                };
                console.log("Post option cycle: "+ post_options)
                // Set up the request
                var post_req = http.request(post_options, function(res) {
                    res.setEncoding('utf8');
                    var responseData = '';
                    res.on('data', function (chunk) {
                        responseData+= chunk;
                    });
                    res.on('end', function(){
                        var obj = JSON.parse(responseData);
                        console.log("res data:  ",responseData );
                        resolve(obj.id);
                    });
                });
                post_req.on('error', (e) => {
                    reject(e);
                });
                // post the data
                post_req.write(post_data);
                post_req.end();
            });
        })
        
            
     }   


    

    this.updateTestResult = function(cycleId,testcaseName, status, imageID,error){
         try{
             return new Promise((resolve, reject) => {
                    createNewExecution(cycleId,testcaseName).then(function(executionId){
                        console.log("Create executionL: " ,executionId, 'for testcase: ' ,testcaseName)
                        updateExecution(cycleId,testcaseName, status,executionId,imageID,error).then(function(status){
                            console.log("Update status: ",status," for executionL: " ,executionId, 'of testcase: ' ,testcaseName)
                            resolve(executionId,status)
                        }).catch(function(e){
                            console("Error: " ,e)
                            reject(e)
                        })
                         
                    })
            });
        }catch(err){
            console.log("catch err: " + err.message);
            return Promise.reject(err);
        }
    }   
    
}


updateExecution = function(cycleId,testcaseName, status, id,imageID,error){
    return exec("java -jar getJWTUpdateExecutions.jar "+id).then(function(rs){
        var stdout = rs.stdout
        text = stdout.substring(stdout.indexOf("JWT KEY:") +8,stdout.length -2)
         console.log("UPdate JWT key: " + stdout.substring(stdout.indexOf("JWT KEY:") +8,stdout.length -2));

        var issueID = testcaseName.split("--")[1]
        var projectID = config.PROJECT_ID;
        var versionID = config.VERSION_ID;
        var zapiAccessKey = config.ZAPI_ACCESS_KEY;
        var objStatus = "";
        var comment = "";
        if(error.length>400){
            error = error.substring(0,400);
        }
        return new Promise((resolve, reject) => {
            var post_data
            if(status == "Passed"){
                post_data = {
                    "issueId": issueID,
                    "cycleId": cycleId,
                    "projectId": projectID,
                    "versionId": versionID,
                    "status": {"id":1},
                    "comment": comment
                };
            }else{
                comment = "error: "+ error+ " picture: http://10.0.0.40:2706/kbr/screenshot/"+imageID
                post_data = {
                    "issueId": issueID,
                    "cycleId": cycleId,
                    "projectId": projectID,
                    "versionId": versionID,
                    "status": {"id":2},
                    "comment": comment
                };
            }
            //objStatus=JSON.stringify(objStatus)
            //body
            
            post_data =  JSON.stringify(post_data)
            console.log("Post data update: "+ post_data)

            // header
            var post_options = {
                host: 'prod-api.zephyr4jiracloud.com',
                port: '443',
                path: '/connect/public/rest/api/1.0/execution/'+id,
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': text,
                    'zapiAccessKey': zapiAccessKey,
                    'Content-Length': Buffer.byteLength(post_data)
                }
            };
            
           console.log("Post option update: "+ post_options)
            var post_req = http.request(post_options, function(res) {
                res.setEncoding('utf8');
                var responseData = '';
                res.on('data', function (chunk) {
                    responseData+= chunk;
                });
                res.on('end', function(){
                    console.log("res update: " , responseData)
                var obj = JSON.parse(responseData);
                console.log("update excution ID :",id ,' To status: ', obj.execution.status.name,' for testcase: ' ,testcaseName );
                resolve(obj.execution.status.name);
                });
            });

            post_req.on('error', (e) => {
                    reject(e);
            });
                // post the data
            post_req.write(post_data);
            post_req.end();
        });

       }); 
}

createNewExecution = function(cycleId,testcaseName){
        var issueID = testcaseName.split("--")[1]
        var projectID = config.PROJECT_ID;
        var versionID = config.VERSION_ID;
        var zapiAccessKey = config.ZAPI_ACCESS_KEY;
        return exec("java -jar getJWTCreateExecutions.jar").then(function(rs){
            var stdout = rs.stdout
            text = stdout.substring(stdout.indexOf("JWT KEY:") +8,stdout.length-2)
            console.log("Create Excution JWT key: " + text);
            return new Promise((resolve, reject) => {
                //body
                var post_data = {
                    "issueId": issueID,
                    "cycleId": cycleId,
                    "projectId": projectID,
                    "versionId": versionID
                };
                 post_data =  JSON.stringify(post_data)
                // header
                var post_options = {
                    host: 'prod-api.zephyr4jiracloud.com',
                    port: '443',
                    path: '/connect/public/rest/api/1.0/execution',
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': text,
                        'zapiAccessKey': zapiAccessKey,
                        'Content-Length': Buffer.byteLength(post_data)
                    }
                };
                console.log("Post option create: "+ post_options)
                // Set up the request
                var post_req = http.request(post_options, function(res) {
                    res.setEncoding('utf8');
                    var responseData = '';
                    res.on('data', function (chunk) {
                        responseData+= chunk;
                    });
                    res.on('end', function(){
                        console.log("res data:  ",responseData );
                        var obj = JSON.parse(responseData);
                        
                        resolve(obj.execution.id);
                    });
                });
                post_req.on('error', (e) => {
                    reject(e);
                });
                // post the data
                post_req.write(post_data);
                post_req.end();

            });
        });
    
}   
    
module.exports = new zephyrintergrate();