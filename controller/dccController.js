
var mysql = require('mysql');
var bodyParser = require('body-parser');
var http =  require("http");
var fs =  require("fs");
var fs_extra = require('fs-extra')
var urlencodeparser = bodyParser.urlencoded({extended: false})
var con_dcc 
var con_dcc2;
var con_dcc3;
var con_dcc4;
var con_dcc5;
var db_auto_config = {
  host: "10.0.0.25",
  user: "root",
  password: "root",
  database:"automation_dcc",
  connectionLimit: 15,
  queueLimit: 30,
  acquireTimeout: 1000000
};

var db_hmte_config = {
  host: "10.0.0.25",
  user: "root",
  password: "root",
  database:"dcc_qa_timesheet",
  connectionLimit: 15,
  queueLimit: 30,
  acquireTimeout: 1000000
};
var db_hmte_project_config = {
  host: "10.0.0.25",
  user: "root",
  password: "root",
  database:"dcc_qa_project",
  connectionLimit: 15,
  queueLimit: 30,
  acquireTimeout: 1000000
};

var db_dcc_config = {
  host: "10.0.0.25",
  user: "root",
  password: "root",
  database:"dcc_stg_timesheet",
  connectionLimit: 15,
  queueLimit: 30,
  acquireTimeout: 1000000
};
var db_dcc_project_config = {
  host: "10.0.0.25",
  user: "root",
  password: "root",
  database:"dcc_stg_project",
  connectionLimit: 15,
  queueLimit: 30,
  acquireTimeout: 1000000
};


function handleDisconnect4() {
  con_dcc4 = mysql.createConnection(db_dcc_config); 
                                                  

  con_dcc4.connect(function(err) {              
    if(err) {                                    
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); 
    }                                    
  });                                     
                                         
  con_dcc4.on('error', function(err) {
    console.log('db error', err);
    if(err.code === 'PROTOCOL_CONNECTION_LOST') { 
      handleDisconnect();                        
    } else {                                      
      throw err;                                 
    }
  });
}
function handleDisconnect5() {
  con_dcc5 = mysql.createConnection(db_dcc_project_config); 
                                                  

  con_dcc5.connect(function(err) {              
    if(err) {                                    
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); 
    }                                    
  });                                     
                                         
  con_dcc5.on('error', function(err) {
    console.log('db error', err);
    if(err.code === 'PROTOCOL_CONNECTION_LOST') { 
      handleDisconnect();                        
    } else {                                      
      throw err;                                 
    }
  });
}
function handleDisconnect() {
  con_dcc = mysql.createConnection(db_auto_config); 
                                                  

  con_dcc.connect(function(err) {              
    if(err) {                                    
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); 
    }                                    
  });                                     
                                         
  con_dcc.on('error', function(err) {
    console.log('db error', err);
    if(err.code === 'PROTOCOL_CONNECTION_LOST') { 
      handleDisconnect();                        
    } else {                                      
      throw err;                                 
    }
  });
}
function handleDisconnect2() {
  con_dcc2 = mysql.createConnection(db_hmte_config); 
                                                  

  con_dcc2.connect(function(err) {              
    if(err) {                                    
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect2, 2000); 
    }                                    
  });                                     
                                         
  con_dcc2.on('error', function(err) {
    console.log('db error', err);
    if(err.code === 'PROTOCOL_CONNECTION_LOST') { 
      handleDisconnect2();                        
    } else {                                      
      throw err;                                 
    }
  });
}
function handleDisconnect3() {
  con_dcc3 = mysql.createConnection(db_hmte_project_config); 
                                                  

  con_dcc3.connect(function(err) {              
    if(err) {                                    
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect3, 2000); 
    }                                    
  });                                     
                                         
  con_dcc3.on('error', function(err) {
    console.log('db error', err);
    if(err.code === 'PROTOCOL_CONNECTION_LOST') { 
      handleDisconnect3();                        
    } else {                                      
      throw err;                                 
    }
  });
}

handleDisconnect();
handleDisconnect2();
handleDisconnect3();
handleDisconnect4();
handleDisconnect5();

module.exports = function(app){
	
		app.get("/dcc/regisnewrun/:buildkey",function(req,res){
		
		con_dcc.query(`INSERT INTO build_key_history(build_key) VALUES ("${req.params.buildkey}")`, function(err1, rows1){
    		if(err1){
        		throw err1;
    		}else{
				/*
				zephyr.createNewCycle().then(function(cycleID){
					console.log("Import CycleID to mysql: " +cycleID)
					con_dcc.query(`INSERT INTO cycle_history(cycle_id) VALUES ("${cycleID}")`, function(err, rows){
						if(err){
							throw err;
						}else{
							console.log('New cycle id have been saved to mysql')
						}
						res.send(rows);
					})
				}).catch(function(err){
					console.log("Error while create new cycle: " +err)
				})

				*/
				res.send(rows1);
			}
    		
		})
	})
app.post("/dcc/postResult",urlencodeparser,function(req,res){
		//res.send("<h1>Hello Anh thái</h1>")
		try{
			var a= req.body.des;
			a = a.split("'").join("");
			a = a.split('"').join("");
			a = a.split('`').join("");
			a = a.split('\\').join("");
			var b= req.body.exception;
			b = b.split("'").join("");
			b = b.split('"').join("");
			b = b.split('`').join("");
			b = b.split('`').join("");
			b = b.split('(').join("");
			b = b.split(')').join("");
			b = b.split('{').join("");
			b = b.split('}').join("");
			b = b.split('\\').join("");
			var c = req.body.image_path.replace("core","");
			
			var e = req.body.stack;
			e = e.split("'").join("");
			e = e.split('"').join("");
			e = e.split('`').join("");
			e = e.split('`').join("");
			e = e.split('(').join("");
			e = e.split(')').join("");
			e = e.split('{').join("");
			e = e.split('}').join("");
			e = e.split('\\').join("");
			if(e.length > 10000){
				e = e.substring(1,1000)
			}
			if(b.length > 1000){
				b = b.substring(1,990)
			}
			selectLastestBuildKeyQuery = "SELECT build_key FROM build_key_history ORDER BY id DESC LIMIT 1";
			con_dcc.query(selectLastestBuildKeyQuery,function(err3,rows3){
				if(err3){
					throw err3;
				}
				var buildkey = rows3[0].build_key	
				var query = "INSERT INTO test_result(build_key,testcase_id,name,result,exception,image_id,message) VALUES('"+buildkey  +"','"+ req.body.idtestlink +"','"+ a +"','"+ req.body.status+"','"+ b +"','"+c+"','"+e+"')";
				console.log(query)
				con_dcc.query(query,function(err, rows){
					if(err){
						throw err;
					}
					console.log("Push test case success");
					/*
					//res.send(`200-put result to database success`);
					selectLastestCycleQuery = "SELECT cycle_id FROM cycle_history ORDER BY id DESC LIMIT 1";
					con_dcc.query(selectLastestCycleQuery,function(err4,rows4){
						if(err4){
							throw err4;
						}else{
							var cycleID = rows4[0].cycle_id
							console.log("Add execution to cycle: " + cycleID)
							zephyr.updateTestResult(cycleID,a+"--"+req.body.idtestlink,req.body.status,c,e).then(function(text){
								console.log("execution id: ", text, " have been created")
								res.send("INSERT INTO test_result(build_key,testcase_id,name,result,exception,image_id,message) VALUES('"+buildkey  +"','"+ req.body.idtestlink +"','"+ a +"','"+ req.body.status+"','"+ b +"','"+c+"','"+e+"')");
							}).catch(function(e){
								console.log("error: " ,e)
							})
						}
					});
					*/
					res.send("Push test case success");
				});
			});
		}catch(errr){
			console.log(errr);
		}   
	})

	app.get("/dcc/report/:buildkey",function(req,res){
		try{
				var html = fs.readFileSync(__dirname+"/reporttemplate/reportBoostrapTemplateDCC.html","utf8");
				var passHTML = fs.readFileSync(__dirname+"/reporttemplate/passTemplateDCC.html","utf8");
				var falseHTML = fs.readFileSync(__dirname+"/reporttemplate/failTemplate.html","utf8");
				var query = "SELECT * FROM test_result WHERE build_key = '"+ req.params.buildkey+"' AND result = 'Passed' "
				var query2 = "SELECT * FROM test_result WHERE build_key = '"+ req.params.buildkey+"' AND result = 'Failed' "
				var date = req.params.buildkey.substring(0, 2)+"-"+req.params.buildkey.substring(2, 4)+"-"+req.params.buildkey.substring(4, 8);
				var time = req.params.buildkey.substring(8, 10)+":"+req.params.buildkey.substring(10, 12);
				var item = "";
				con_dcc.query(query,function(err,rows){
					if(err){
						throw err;
					}
					con_dcc.query(query2,function(err2,rows2){
						//res.writeHead(200,{'Conten-Type':'text/html'})
						if(err2){
							throw err2;
						}
						html = html.split('$numberPass').join(rows.length);
						html = html.split('$numberFail').join(rows2.length);
						html = html.split('$numberTestcase').join(rows.length+rows2.length);
						html = html.split('$startTime').join(time);
						html = html.split('$date').join(date);
						var items = "";
						var ret = [];
						for(i=0;i<rows.length;i++){
							item = passHTML.split('$headingPass').join("headingPass"+i)
							item = item.split('$collapsePass').join("collapsePass"+i)
							item = item.split('$testcaseName').join(rows[i].name)
							items = items+item
						}
						html = html.split('$pass').join(items);
						items = "";
						for(i=0;i<rows2.length;i++){
							item = falseHTML.split('$headingFail').join("headingFail"+i)
							item = item.split('$collapseFail').join("collapseFail"+i)
							item = item.split('$message').join(rows2[i].message)
							item = item.split('$testcaseName').join(rows2[i].name)
							item = item.split('$image').join(req.protocol + '://' + req.get('host') +"/kbr/screenshot/"+rows2[i].image_id)
							item = item.split('$log').join(rows2[i].exception)
							items = items+item
						}
						html = html.split('$fail').join(items);
						var optionQuery = "SELECT build_key FROM build_key_history ORDER BY id DESC"
							con_dcc.query(optionQuery,function(err4,rows4){
								console.log(rows4)
								var items = "";
								for(i=0;i<rows4.length;i++){
									items = items+"<option>"+rows4[i].build_key+"</option>"
								}
								console.log(items)
								html = html.split('$options').join(items);
								res.send(html)
							})
					});	
				});				
			}catch(errr){
				console.log(errr);
			}
	})

	app.get("/dcc/lastest",function(req,res){
		try{
			selectLastestBuildKeyQuery = "SELECT build_key FROM build_key_history ORDER BY id DESC LIMIT 1";
				con_dcc.query(selectLastestBuildKeyQuery,function(err3,rows3){
					if(err3){
						throw err3;
					}
					var html = fs.readFileSync(__dirname+"/reporttemplate/reportBoostrapTemplateDCC.html","utf8");
					var passHTML = fs.readFileSync(__dirname+"/reporttemplate/passTemplateDCC.html","utf8");
					var falseHTML = fs.readFileSync(__dirname+"/reporttemplate/failTemplate.html","utf8");
					var query = "SELECT * FROM test_result WHERE build_key = '"+ rows3[0].build_key+"' AND result = 'Passed' "
					var query2 = "SELECT * FROM test_result WHERE build_key = '"+ rows3[0].build_key+"' AND result = 'Failed' "
					var date = rows3[0].build_key.substring(0, 2)+"-"+rows3[0].build_key.substring(2, 4)+"-"+rows3[0].build_key.substring(4, 8);
					var time = rows3[0].build_key.substring(8, 10)+":"+rows3[0].build_key.substring(10, 12);
					var item = "";
					con_dcc.query(query,function(err,rows){
						if(err){
							throw err;
						}
						con_dcc.query(query2,function(err2,rows2){
							//res.writeHead(200,{'Conten-Type':'text/html'})
							if(err2){
								throw err2;
							}
							html = html.split('$numberPass').join(rows.length);
							html = html.split('$numberFail').join(rows2.length);
							html = html.split('$numberTestcase').join(rows.length+rows2.length);
							html = html.split('$startTime').join(time);
							html = html.split('$date').join(date);
							var items = "";
							var ret = [];
							for(i=0;i<rows.length;i++){
								item = passHTML.split('$headingPass').join("headingPass"+i)
								item = item.split('$collapsePass').join("collapsePass"+i)
								item = item.split('$testcaseName').join(rows[i].name)
								items = items+item
							}
							html = html.split('$pass').join(items);
							items = "";
							for(i=0;i<rows2.length;i++){
								item = falseHTML.split('$headingFail').join("headingFail"+i)
								item = item.split('$collapseFail').join("collapseFail"+i)
								item = item.split('$message').join(rows2[i].message)
								item = item.split('$testcaseName').join(rows2[i].name)
								item = item.split('$image').join(req.protocol + '://' + req.get('host') +"/kbr/screenshot/"+rows2[i].image_id)
								item = item.split('$log').join(rows2[i].exception)
								items = items+item
							}
							html = html.split('$fail').join(items);
							var optionQuery = "SELECT build_key FROM build_key_history ORDER BY id DESC"
							con_dcc.query(optionQuery,function(err4,rows4){
								console.log(rows4)
								var items = "";
								for(i=0;i<rows4.length;i++){
									items = items+"<option>"+rows4[i].build_key+"</option>"
								}
								console.log(items)
								html = html.split('$options').join(items);
								res.send(html)
							})
						});	
					});
				});
			}catch(errr){
				console.log(errr);
			}
	})



	app.get("/dcc/deleteTimesheet/:userID",function(req,res){
		var userid=req.params.userID;
		var deleteTimesheetHistory = `DELETE FROM user_timesheet_history WHERE user_id = '${userid}'`
		var deleteTimesheetEntry = `DELETE FROM timesheet_entry WHERE user_id = '${userid}'`
		exequeryquery(con_dcc4,deleteTimesheetHistory).then(exequeryquery(con_dcc4,deleteTimesheetEntry)).then(function(){
			console.log("Delete All")
				res.send("success")
		}).catch(function(err){
			console.log("err::: ", err)
						res.send("err")
		});
	})

	app.get("/dcc/updateRole/:userID/:newRole",function(req,res){
		var userid=req.params.userID;
		var role=req.params.newRole;
		var updateRole = `UPDATE users_roles SET role_id = ${role} WHERE user_id = ${userid}`
		exequeryquery(con_dcc4,updateRole).then(function(){
			console.log("Delete All")
				res.send("success")
		}).catch(function(err){
			console.log("err::: ", err)
						res.send("err")
		});
	})
	
	app.get("/dcc/api",function(req,res){
		con_dcc2.query("SELECT * FROM user WHERE email = 'chanhleo1@gmail.com' ", function(err, rows){
    		if(err){
        		throw err;
    		}
    	res.send(rows);
		})
	})
	app.get("/dcc/api2",function(req,res){
		con_dcc.query("SELECT * FROM test_result WHERE id_testlink = '20461' ", function(err, rows){
    		if(err){
        		throw err;
    		}
    	res.send(rows);
		})
	})
	app.get("/dcc/getProject/:user/:type",function(req,res){
		var query = `SELECT project.name FROM project, project_approval 
								 WHERE project.id = project_approval.project_id 
								 AND project_approval.employee_id = '${req.params.user}'
								AND project.type = '${req.params.type}' ORDER BY project.name DESC LIMIT 1`;
			console.log(query)
		con_dcc3.query(query, function(err, rows){
    		if(err){
        		throw err;
    		}
    	res.send(rows);
		})
	})
	
		app.get("/dcc/getMilestone/:projectId",function(req,res){
		var query = `SELECT COUNT(*) AS count FROM milestone WHERE project_id = ${req.params.projectId}`;
			console.log(query)
		con_dcc3.query(query, function(err, rows){
    		if(err){
        		throw err;
    		}
    	res.send(rows);
		})
	})

	app.get("/dcc/getMilestoneDate/:msdate",function(req,res){
		var query = `SELECT planned_date AS date FROM milestone WHERE code ='${req.params.msdate}'`;
			console.log(query)
		con_dcc3.query(query, function(err, rows){
    		if(err){
        		throw err;
    		}
    	res.send(rows);
		})
	})
	
	
	app.get("/dcc/getEmployeeByType/:type",function(req,res){
		var query = `select count(*) as count from employee where employee.disable = 0 and user_type = '${req.params.type}'`;
			console.log(query)
		con_dcc3.query(query, function(err, rows){
    		if(err){
        		throw err;
    		}
    	res.send(rows);
		})
	})
	
	app.get("/dcc/getLiveProjectByLocation/:location",function(req,res){
		var query = `select count(*) as count from project where office_location = '${req.params.location}'AND actual_start_date BETWEEN 1483228800000 AND 1514678400000 `;
			console.log(query)
		con_dcc3.query(query, function(err, rows){
    		if(err){
        		throw err;
    		}
    	res.send(rows);
		})
	})

	app.get("/dcc/getProjectVisualizer",function(req,res){
		var query = `select count(*) as count FROM project where 
					id in (
					select id from project where type = 'OPPORTUNITY' and sow_start_date <= 1514739599999 and sow_end_date >= 1483290000000 and sow_start_date is not null and sow_end_date is not null 
					UNION
					select id from project where type = 'LIVE' and status <> 'NOT_STARTED'  and actual_start_date <= 1514739599999 and actual_end_date >= 1483290000000 and actual_start_date is not null and actual_end_date is not null 
					UNION
					select id from project where type = 'LIVE' and status = 'NOT_STARTED'  and actual_start_date <= 1514739599999 and actual_end_date >= 1483290000000 and sow_start_date is not null and sow_end_date is not null )`;
			console.log(query)
		con_dcc3.query(query, function(err, rows){
    		if(err){
        		throw err;
    		}
    	res.send(rows);
		})
	})

		app.get("/dcc/getLiveProjectByType/:type",function(req,res){
		var query = `select count(*) as count FROM project where 
					id in (
					select id from project where type = '${req.params.type}' and sow_start_date <= 1514739599999 and sow_end_date >= 1483290000000 and sow_start_date is not null and sow_end_date is not null 
			)`;
			console.log(query)
		con_dcc3.query(query, function(err, rows){
    		if(err){
        		throw err;
    		}
    	res.send(rows);
		})
	})

	app.get("/dcc/getTypeProject/:project",function(req,res){
		var query = `select type from project WHERE code = '${req.params.project}'`;
			console.log(query)
		con_dcc3.query(query, function(err, rows){
    		if(err){
        		throw err;
    		}
    	res.send(rows);
		})
	})
	app.get("/dcc/getLiveProjectByStatus/:status",function(req,res){
		var query = `select count(*) as count FROM project where 
					id in (
					select id from project where type = 'LIVE' and status = '${req.params.status}' and sow_start_date <= 1514739599999 and sow_end_date >= 1483290000000 and sow_start_date is not null and sow_end_date is not null 
			)`;
			console.log(query)
		con_dcc3.query(query, function(err, rows){
    		if(err){
        		throw err;
    		}
    	res.send(rows);
		})
	})
	
	app.get("/dcc/deleteLeave1/:userid",function(req,res){
		var selectRequestID = `SELECT request_id FROM leave_request WHERE user_id = '${req.params.userid}'`
		var deleteAttachQuery = `DELETE FROM leave_attachment WHERE request_id = '{Param}'`
		var deleteApproveQuery = `DELETE FROM approver_request WHERE request_id = '{Param}'`
		var deleteRequestQuery = `DELETE FROM leave_request WHERE request_id = '{Param}'`
		con_dcc2.query(selectRequestID, function(err1,row1){
			if(err1){
        		throw err1;
    		}
			console.log("User ",req.params.userid," have  ",row1.length, " leave requests")
			for(i=0;i<row1.length;i++){
				var index = row1[i].request_id;
				console.log(deleteAttachQuery.replace("{Param}",row1[i].request_id))
				con_dcc2.query(deleteAttachQuery.replace("{Param}",row1[i].request_id), function(err2,row2){
					if(err2){
						throw err2;
					}else{
						console.log(deleteApproveQuery.replace("{Param}",row1[i].request_id))
						con_dcc2.query(deleteApproveQuery.replace("{Param}",row1[i].request_id), function(err3,row3){
							if(err3){
								throw err3;
							}else{
								console.log(deleteRequestQuery.replace("{Param}",row1[i].request_id))
								con_dcc2.query(deleteRequestQuery.replace("{Param}",row1[i].request_id), function(err4,row4){
									if(err4){
										throw err4;
									}else{
										res.send("Deleted")
									}
								});
							}
						});
					}
					
				});
			}
		})
	})

	app.get("/dcc/deleteLeave/:userid",function(req,res){
		var selectRequestID = `SELECT request_id FROM leave_request WHERE user_id = '${req.params.userid}'`
		var deleteAttachQuery = `DELETE FROM leave_attachment WHERE request_id = '{Param}'`
		var deleteApproveQuery = `DELETE FROM approver_request WHERE request_id = '{Param}'`
		var deleteRequestQuery = `DELETE FROM leave_request WHERE request_id = '{Param}'`
		con_dcc2.query(selectRequestID, function(err1,row1){
			if(err1){
        		throw err1;
    		}
			console.log("User ",req.params.userid," have  ",row1.length, " leave requests")
			for(i=0;i<row1.length;i++){
				var index = row1[i].request_id;
				exequeryquery(con_dcc2,deleteAttachQuery.replace("{Param}",row1[i].request_id)).then(exequeryquery(con_dcc2,deleteApproveQuery.replace("{Param}",row1[i].request_id)))
																							.then(exequeryquery(con_dcc2,deleteRequestQuery.replace("{Param}",row1[i].request_id)))
																							.then(function(){
																								console.log("pass heet");
																							}).catch(function(err){
																								console.log(err);
																								res.send(err)
																							})
			}
			res.send("success")
		})
	})

exequeryquery = function(con,query){
   return new Promise((resolve, reject) => {
	   console.log(query)
	   con.query(query, function(err,row){
			if(err){
				reject(err);
			}else{
				resolve(row);
			}
	   });
    });
}



/*	app.get("/getLiveProject/:from/:to",function(req,res){
		var query = `select count(*) as count from project WHERE actual_start_date <= ${req.params.to} OR actual_end_date >= ${req.params.from} `;
		console.log(query)
		con3.query(query, function(err, rows){
    		if(err){
        		throw err;
    		}
    	res.send(rows);
		})
	}) */

	app.get("/dcc/getLiveProject/:from/:to",function(req,res){
		var query = `select count(*) as count FROM project where 
					id in (
					select id from project where type = 'OPPORTUNITY' and sow_start_date <= ${req.params.to} and sow_end_date >= ${req.params.from} and sow_start_date is not null and sow_end_date is not null 
					UNION
					select id from project where type = 'LIVE' and status <> 'NOT_STARTED'  and actual_start_date <= ${req.params.to} and actual_end_date >= ${req.params.from} and actual_start_date is not null and actual_end_date is not null 
					UNION
					select id from project where type = 'LIVE' and status = 'NOT_STARTED'  and actual_start_date <= ${req.params.to} and actual_end_date >= ${req.params.from} and sow_start_date is not null and sow_end_date is not null )`;
			console.log(query)
		con_dcc3.query(query, function(err, rows){
    		if(err){
        		throw err;
    		}
    	res.send(rows);
		})
	})


	app.get("/dcc/getAllEmployeeProject/:proID",function(req,res){
		var query = `select count(*) as count from allocation where project_id = '${req.params.proID}'`;
			console.log(query)
		con_dcc3.query(query, function(err, rows){
    		if(err){
        		throw err;
    		}
    	res.send(rows);
		})
	})
	app.get("/dcc/getAllEmployee",function(req,res){
		var query = `select count(*) as count from employee where employee.disable = 0`;
			console.log(query)
		con_dcc3.query(query, function(err, rows){
    		if(err){
        		throw err;
    		}
    	res.send(rows);
		})
	})
	app.get("/dcc/getMilestoneStatus/:msid",function(req,res){
		var query = `SELECT status FROM milestone WHERE code ='${req.params.msid}'`;
			console.log(query)
		con_dcc3.query(query, function(err, rows){
    		if(err){
        		throw err;
    		}
    	res.send(rows);
		})
	})
	app.get("/dcc/getTodayLeave/:location",function(req,res){
		var query = `SELECT  COUNT(*) AS num FROM leave_request,user WHERE ADDTIME(now(), '07:00:00')  > start_date AND ADDTIME(now(), '07:00:00')  < end_date AND office_location = '${req.params.location}' and leave_request_status = 'APPROVED' 	and user.user_id = leave_request.user_id`;
			console.log(query)
		con_dcc2.query(query, function(err, rows){
    		if(err){
        		throw err;
    		}
    	res.send(rows);
		})
	})
	app.get("/dcc/getTodayLeaveAdmin",function(req,res){
		var query = `SELECT  COUNT(*) AS num FROM leave_request,user WHERE  ADDTIME(now(), '07:00:00')  > start_date AND ADDTIME(now(), '07:00:00')  < end_date and leave_request_status = 'APPROVED' 	and user.user_id = leave_request.user_id`;
			console.log(query)
		con_dcc2.query(query, function(err, rows){
    		if(err){
        		throw err;
    		}
    	res.send(rows);
		})
	})
	app.get("/dcc/getRemainingTime/:email",function(req,res){
		console.log(`SELECT remaining_leave_day FROM user WHERE email = '${req.params.email}' `)
		con_dcc2.query(`SELECT remaining_leave_day FROM user WHERE email = '${req.params.email} '`, function(err, rows){
    		if(err){
        		throw err;
    		}
    	res.send(rows);
		})
	})
	app.get("/dcc/getUnavaiableTime/:userid",function(req,res){
		console.log(`SELECT SUM(duration) FROM leave_request where user_id ='${req.params.userid}' and leave_request_status = 'APPROVED'`)
		con_dcc2.query(`SELECT SUM(duration) FROM leave_request where user_id ='${req.params.userid}' and leave_request_status = 'APPROVED'`, function(err, rows){
    		if(err){
        		throw err;
    		}
    	res.send(rows);
		})
	})
	app.get("/dcc",function(req,res){
    	res.send("<h1>Hello Anh thái</h1>")
	})
	/*
	app.post("/dcc/postResult",urlencodeparser,function(req,res){
		//res.send("<h1>Hello Anh thái</h1>")
		try{
			var a= req.body.des;
			a = a.split("'").join("");
			a = a.split('"').join("");
			a = a.split('`').join("");
			a = a.split('\\').join("");
			var b= req.body.exception;
			b = b.split("'").join("");
			b = b.split('"').join("");
			b = b.split('`').join("");
			b = b.split('\\').join("");
			c = req.body.image_path.replace("core","");
			var d = __dirname+"\\testresult\\"+req.body.buildid +"\\"+req.body.idtestlink+".png";
			d = d.split('\\').join("\\\\");
			fs_extra.copy(c, d).then(() => console.log('success!')).catch(err => console.error(err))
			d = "/testresult/"+req.body.buildid +"/"+req.body.idtestlink+".png";
			e = req.body.stack;
			e = e.split("'").join("");
			e = e.split('"').join("");
			e = e.split('`').join("");
			e = e.split('`').join("");
			e = e.split('(').join("");
			e = e.split(')').join("");
			e = e.split('{').join("");
			e = e.split('}').join("");
			e = e.split('\\').join("");
			if(e.length > 10000){
				e = e.substring(1,1000)
			}
			if(b.length > 1000){
				b = b.substring(1,990)
			}
			var query = "INSERT INTO test_result(id_testlink,build_key,name_test,result,exeption,image_path,stack) VALUES('"+req.body.idtestlink  +"','"+ req.body.buildid +"','"+ a +"','"+ req.body.status+"','"+ b +"','"+d+"','"+e+"')";
			console.log(query)
			con_dcc.query(query,function(err){
			 	if(err){
        			throw err;
    			}
		 	});
    		res.send(`200-put result to database success`);
			console.log("Push test case " +req.body.idtestlink + "success");
			
		}catch(errr){
			console.log(errr);
		}   
	})
	*/
	app.post("/dcc/postLeave",urlencodeparser,function(req,res){
		try{
			
			var query = "INSERT INTO leave_request(user,type,status,des) VALUES('"+req.body.user  +"','"+ req.body.type  +"','"+ req.body.status+"','"+ req.body.des +"')";
			console.log(query)
			con_dcc.query(query,function(err){
			 	if(err){
        			throw err;
    			}
		 	});
    	res.send(`200-put reslaveult to database success`);
			console.log("Push leave " +req.body.des + "success");
			
		}catch(errr){
			console.log(errr);
		}   
	})
	app.get("/dcc/getLeave/:email/:status",function(req,res){
		console.log(`SELECT * FROM leave_request WHERE user = '${req.params.email}' AND status = '${req.params.status}' ORDER BY id LIMIT 1 `)
		con_dcc.query(`SELECT * FROM leave_request WHERE user = '${req.params.email}' AND status = '${req.params.status}' ORDER BY id LIMIT 1`, function(err, rows){
    		if(err){
        		throw err;
    		}
    	res.send(rows);
		})
	})
	app.get("/dcc/updateLeave/:status/:des",function(req,res){
		console.log(`UPDATE leave_request SET status = '${req.params.status}' WHERE des = '${req.params.des}'`)
		con_dcc.query(`UPDATE leave_request SET status = '${req.params.status}' WHERE des = '${req.params.des}' `, function(err, rows){
    		if(err){
        		throw err;
    		}
    	res.send(rows);
		})
	})
	app.get("/dcc/compare/:date/:currentBuildKey",function(req,res){

		var tagetDate = `${req.params.date}`;
		tagetDate = tagetDate.split("-");
		var queryGetBuildTaget = `SELECT ValidBuildKey FROM build_key_history WHERE ValidBuildKey LIKE '`+tagetDate[2]+tagetDate[1]+`%';`
		var currentBuildKey =`${req.params.currentBuildKey}`
		//res.send(`${req.params.date}`);
		console.log(queryGetBuildTaget)
		con_dcc.query(queryGetBuildTaget, function(err, rows){
    		if(err){
        		throw err;
    		}
			
				console.log("build key1 ",rows)
				console.log(`build key2 `,currentBuildKey)
				if(rows.length == 0){
					res.send("<h3>Do not have automation test data for selected date</h3>")
				}else{
					var query3 = "SELECT * FROM test_result WHERE build_key = '"+ rows[0].ValidBuildKey+"' AND result = 'passed' AND id_testlink <> ''"
					var query4 = "SELECT * FROM test_result WHERE build_key = '"+ rows[0].ValidBuildKey+"' AND result = 'failed' AND id_testlink <> ''"			
					var query5 = "SELECT * FROM test_result WHERE build_key = '"+ currentBuildKey+"' AND result = 'passed' AND id_testlink <> ''"
					var query6 = "SELECT * FROM test_result WHERE build_key = '"+ currentBuildKey+"' AND result = 'failed' AND id_testlink <> ''"	
					
					con_dcc.query(query3, function(err3, tagetPass){
						if(err3){
							throw err3;
						}
						con_dcc.query(query4, function(err4, tagetFalse){
							if(err3){
								throw err3;
							}
							con_dcc.query(query5, function(err5, todayPass){
								if(err5){
									throw err5;
								}
								con_dcc.query(query6, function(err6, todayFalse){
									if(err6){
										throw err6;
									}
									for(i=0;i<todayPass.length;i++){
											for(j=0;j<tagetPass.length;j++){
												//console.log()
													if(tagetPass[j].id_testlink == todayPass[i].id_testlink){
														todayPass[i].id_testlink = "";
													}
											}
									}
									for(i=0;i<todayFalse.length;i++){
											for(j=0;j<tagetFalse.length;j++){
													if(tagetFalse[j].id_testlink == todayFalse[i].id_testlink){
														todayFalse[i].id_testlink = ""
													}
											}
									}
									var countPass = 0;
									var countFail = 0
									for(i=0;i<todayPass.length;i++){
										if(todayPass[i].id_testlink != ""){
											countPass++;
										}
									}
									for(i=0;i<todayFalse.length;i++){
										if(todayFalse[i].id_testlink != ""){
											countFail++;
										}
									}
									var passHTML = fs.readFileSync(__dirname+"/reporttemplate/passTemplate.html","utf8");
									var falseHTML = fs.readFileSync(__dirname+"/reporttemplate/failTemplate.html","utf8");
									var compareHTML = fs.readFileSync(__dirname+"/reporttemplate/compare.html","utf8");	
									compareHTML = compareHTML.split('$numberPass').join(countPass);
									compareHTML = compareHTML.split('$numberFail').join(countFail);
									var items = "";
									for(i=0;i<todayPass.length;i++){
										if(todayPass[i].id_testlink != ""){
											item = passHTML.split('$headingPass').join("headingPass"+i+"Com")
											item = item.split('$collapsePass').join("collapsePass"+i+"Com")
											item = item.split('$testcaseName').join(todayPass[i].name_test)
											item = item.split('$image').join(todayPass[i].image_path)
											item = item.split('#accordionPass').join('#accordionPassCom')
											items = items+item
										}
									}
									compareHTML = compareHTML.split('$passCom').join(items);
									items = "";
									for(i=0;i<todayFalse.length;i++){
										if(todayFalse[i].id_testlink != ""){
											item = falseHTML.split('$headingFail').join("headingFail"+i+"Com")
											item = item.split('$collapseFail').join("collapseFail"+i+"Com")
											item = item.split('$message').join(todayFalse[i].exeption)
											item = item.split('$testcaseName').join(todayFalse[i].name_test)
											item = item.split('$image').join(todayFalse[i].image_path)
											item = item.split('$log').join(todayFalse[i].stack)
											item = item.split('#accordionFail').join('#accordionFailCom')
											items = items+item
										}
									}
									compareHTML = compareHTML.split('$failCom').join(items);
									res.send(compareHTML)


								})
							})
						})
					})
				}
			
		})
		
	})
	app.get("/dcc/compareToday/:date",function(req,res){

		var tagetDate = `${req.params.date}`;
		tagetDate = tagetDate.split("-");
		var selectLastestBuildKeyQuery = "SELECT ValidBuildKey FROM build_key_history ORDER BY id DESC LIMIT 1";
		var queryGetBuildTaget = `SELECT ValidBuildKey FROM build_key_history WHERE ValidBuildKey LIKE '`+tagetDate[2]+tagetDate[1]+`%';`
		//res.send(`${req.params.date}`);
		console.log(queryGetBuildTaget)
		con_dcc.query(queryGetBuildTaget, function(err, rows){
    		if(err){
        		throw err;
    		}
			con_dcc.query(selectLastestBuildKeyQuery, function(err, rows2){
				if(err){
					throw err;
				}
				var currentBuildKey =rows2[0].ValidBuildKey
				console.log("build key1 ",rows)
				console.log(`build key2 `,currentBuildKey)
				if(rows.length == 0){
					res.send("<h3>Do not have automation test data for selected date</h3>")
				}else{
					var query3 = "SELECT * FROM test_result WHERE build_key = '"+ rows[0].ValidBuildKey+"' AND result = 'passed' AND id_testlink <> ''"
					var query4 = "SELECT * FROM test_result WHERE build_key = '"+ rows[0].ValidBuildKey+"' AND result = 'failed' AND id_testlink <> ''"			
					var query5 = "SELECT * FROM test_result WHERE build_key = '"+ currentBuildKey+"' AND result = 'passed' AND id_testlink <> ''"
					var query6 = "SELECT * FROM test_result WHERE build_key = '"+ currentBuildKey+"' AND result = 'failed' AND id_testlink <> ''"	
					con_dcc.query(query3, function(err3, tagetPass){
						if(err3){
							throw err3;
						}
						con_dcc.query(query4, function(err4, tagetFalse){
							if(err3){
								throw err3;
							}
							con_dcc.query(query5, function(err5, todayPass){
								if(err5){
									throw err5;
								}
								con_dcc.query(query6, function(err6, todayFalse){
									if(err6){
										throw err6;
									}
									for(i=0;i<todayPass.length;i++){
											for(j=0;j<tagetPass.length;j++){
													if(tagetPass[j].id_testlink == todayPass[i].id_testlink){
														todayPass[i].id_testlink = "";
													}
											}
									}
									for(i=0;i<todayFalse.length;i++){
											for(j=0;j<tagetFalse.length;j++){
													if(tagetFalse[j].id_testlink == todayFalse[i].id_testlink){
														todayFalse[i].id_testlink = ""
													}
											}
									}
									var countPass = 0;
									var countFail = 0
									for(i=0;i<todayPass.length;i++){
										if(todayPass[i].id_testlink != ""){
											countPass++;
										}
									}
									for(i=0;i<todayFalse.length;i++){
										if(todayFalse[i].id_testlink != ""){
											countFail++;
										}
									}
									var passHTML = fs.readFileSync(__dirname+"/reporttemplate/passTemplate.html","utf8");
									var falseHTML = fs.readFileSync(__dirname+"/reporttemplate/failTemplate.html","utf8");
									var compareHTML = fs.readFileSync(__dirname+"/reporttemplate/compare.html","utf8");	
									compareHTML = compareHTML.split('$numberPass').join(countPass);
									compareHTML = compareHTML.split('$numberFail').join(countFail);
									var items = "";
									for(i=0;i<todayPass.length;i++){
										if(todayPass[i].id_testlink != ""){
											item = passHTML.split('$headingPass').join("headingPass"+i+"Com")
											item = item.split('$collapsePass').join("collapsePass"+i+"Com")
											item = item.split('$testcaseName').join(todayPass[i].name_test)
											item = item.split('$image').join(todayPass[i].image_path)
											item = item.split('#accordionPass').join('#accordionPassCom')
											items = items+item
										}
									}
									compareHTML = compareHTML.split('$passCom').join(items);
									items = "";
									for(i=0;i<todayFalse.length;i++){
										if(todayFalse[i].id_testlink != ""){
											item = falseHTML.split('$headingFail').join("headingFail"+i+"Com")
											item = item.split('$collapseFail').join("collapseFail"+i+"Com")
											item = item.split('$message').join(todayFalse[i].exeption)
											item = item.split('$testcaseName').join(todayFalse[i].name_test)
											item = item.split('$image').join(todayFalse[i].image_path)
											item = item.split('$log').join(todayFalse[i].stack)
											item = item.split('#accordionFail').join('#accordionFailCom')
											items = items+item
										}
									}
									compareHTML = compareHTML.split('$failCom').join(items);
									res.send(compareHTML)


								})
							})
						})
					})
				}
			})
		})
		
	})
	app.get("/dcc/buildKey/:build", function(req,res){
		
		try{
			var html = fs.readFileSync(__dirname+"/reporttemplate/reportBoostrapTemplate.html","utf8");
			var passHTML = fs.readFileSync(__dirname+"/reporttemplate/passTemplate.html","utf8");
			var falseHTML = fs.readFileSync(__dirname+"/reporttemplate/failTemplate.html","utf8");
			var query = "SELECT * FROM test_result WHERE build_key = '"+ req.params.build+"' AND result = 'passed' AND id_testlink <> ''"
			var query2 = "SELECT * FROM test_result WHERE build_key = '"+ req.params.build+"' AND result = 'failed' AND id_testlink <> ''"
			var date = req.params.build.substring(0, 2)+"-"+req.params.build.substring(2, 4)+"-"+req.params.build.substring(4, 8);
			var time = req.params.build.substring(8, 10)+":"+req.params.build.substring(10, 12);
			var item = "";
			con_dcc.query(query,function(err,rows){
			 	if(err){
        			throw err;
    			}
				con_dcc.query(query2,function(err2,rows2){
					//res.writeHead(200,{'Conten-Type':'text/html'})
					if(err2){
        				throw err2;
    				}
					html = html.split('$numberPass').join(rows.length);
					html = html.split('$numberFail').join(rows2.length);
					html = html.split('$numberTestcase').join(rows.length+rows2.length);
					html = html.split('$startTime').join(time);
					html = html.split('$date').join(date);
					var items = "";
					var ret = [];
					for(i=0;i<rows.length;i++){
						item = passHTML.split('$headingPass').join("headingPass"+i)
						item = item.split('$collapsePass').join("collapsePass"+i)
						item = item.split('$testcaseName').join(rows[i].name_test)
						item = item.split('$image').join(rows[i].image_path)
						items = items+item
					}
					html = html.split('$pass').join(items);
					items = "";
					for(i=0;i<rows2.length;i++){
						item = falseHTML.split('$headingFail').join("headingFail"+i)
						item = item.split('$collapseFail').join("collapseFail"+i)
						item = item.split('$message').join(rows2[i].exeption)
						item = item.split('$testcaseName').join(rows2[i].name_test)
						item = item.split('$image').join(rows2[i].image_path)
						item = item.split('$log').join(rows2[i].stack)
						items = items+item
					}
					html = html.split('$fail').join(items);
					res.send(html)
				});	
				
		 	});
		}catch(errr){
			console.log(errr);
		}
     	
	})

	app.get("/dcc/TodayReport", function(req,res){
		
		try{

			selectLastestBuildKeyQuery = "SELECT ValidBuildKey FROM build_key_history ORDER BY id DESC LIMIT 1";
			con_dcc.query(selectLastestBuildKeyQuery,function(err3,rows3){
			 	if(err3){
        			throw err3;
    			}
				var html = fs.readFileSync(__dirname+"/reporttemplate/reportBoostrapTemplate.html","utf8");
				var passHTML = fs.readFileSync(__dirname+"/reporttemplate/passTemplate.html","utf8");
				var falseHTML = fs.readFileSync(__dirname+"/reporttemplate/failTemplate.html","utf8");
				var query = "SELECT * FROM test_result WHERE build_key = '"+ rows3[0].ValidBuildKey+"' AND result = 'passed' AND id_testlink <> ''"
				var query2 = "SELECT * FROM test_result WHERE build_key = '"+ rows3[0].ValidBuildKey+"' AND result = 'failed' AND id_testlink <> ''"
				var date = rows3[0].ValidBuildKey.substring(0, 2)+"-"+rows3[0].ValidBuildKey.substring(2, 4)+"-"+rows3[0].ValidBuildKey.substring(4, 8);
				var time = rows3[0].ValidBuildKey.substring(8, 10)+":"+rows3[0].ValidBuildKey.substring(10, 12);
				var item = "";
				con_dcc.query(query,function(err,rows){
					if(err){
						throw err;
					}
					con_dcc.query(query2,function(err2,rows2){
						//res.writeHead(200,{'Conten-Type':'text/html'})
						if(err2){
							throw err2;
						}
						html = html.split('$numberPass').join(rows.length);
						html = html.split('$numberFail').join(rows2.length);
						html = html.split('$numberTestcase').join(rows.length+rows2.length);
						html = html.split('$startTime').join(time);
						html = html.split('$date').join(date);
						var items = "";
						var ret = [];
						for(i=0;i<rows.length;i++){
							item = passHTML.split('$headingPass').join("headingPass"+i)
							item = item.split('$collapsePass').join("collapsePass"+i)
							item = item.split('$testcaseName').join(rows[i].name_test)
							item = item.split('$image').join(rows[i].image_path)
							items = items+item
						}
						html = html.split('$pass').join(items);
						items = "";
						for(i=0;i<rows2.length;i++){
							item = falseHTML.split('$headingFail').join("headingFail"+i)
							item = item.split('$collapseFail').join("collapseFail"+i)
							item = item.split('$message').join(rows2[i].exeption)
							item = item.split('$testcaseName').join(rows2[i].name_test)
							item = item.split('$image').join(rows2[i].image_path)
							item = item.split('$log').join(rows2[i].stack)
							items = items+item
						}
						html = html.split('$fail').join(items);
						res.send(html)
					});	
				});
		 	});
		}catch(errr){
			console.log(errr);
		}
     	
	})

		app.get("/dcc/TodayReportMobile", function(req,res){
		
		try{

			selectLastestBuildKeyQuery = "SELECT ValidBuildKey FROM build_key_history ORDER BY id DESC LIMIT 1";
			con_dcc.query(selectLastestBuildKeyQuery,function(err3,rows3){
			 	if(err3){
        			throw err3;
    			}
				var html = fs.readFileSync(__dirname+"/reporttemplate/reportBoostrapTemplateMobile.html","utf8");
				var passHTML = fs.readFileSync(__dirname+"/reporttemplate/passTemplate.html","utf8");
				var falseHTML = fs.readFileSync(__dirname+"/reporttemplate/failTemplate.html","utf8");
				var query = "SELECT * FROM test_result WHERE build_key = '"+ rows3[0].ValidBuildKey+"' AND result = 'passed' AND id_testlink <> ''"
				var query2 = "SELECT * FROM test_result WHERE build_key = '"+ rows3[0].ValidBuildKey+"' AND result = 'failed' AND id_testlink <> ''"
				var date = rows3[0].ValidBuildKey.substring(0, 2)+"-"+rows3[0].ValidBuildKey.substring(2, 4)+"-"+rows3[0].ValidBuildKey.substring(4, 8);
				var time = rows3[0].ValidBuildKey.substring(8, 10)+":"+rows3[0].ValidBuildKey.substring(10, 12);
				var item = "";
				con_dcc.query(query,function(err,rows){
					if(err){
						throw err;
					}
					con_dcc.query(query2,function(err2,rows2){
						//res.writeHead(200,{'Conten-Type':'text/html'})
						if(err2){
							throw err2;
						}
						html = html.split('$numberPass').join(rows.length);
						html = html.split('$numberFail').join(rows2.length);
						html = html.split('$numberTestcase').join(rows.length+rows2.length);
						html = html.split('$startTime').join(time);
						html = html.split('$date').join(date);
						var items = "";
						var ret = [];
						for(i=0;i<rows.length;i++){
							item = passHTML.split('$headingPass').join("headingPass"+i)
							item = item.split('$collapsePass').join("collapsePass"+i)
							item = item.split('$testcaseName').join(rows[i].name_test)
							item = item.split('$image').join("http://10.0.0.40:2706"+rows[i].image_path)
							items = items+item
						}
						html = html.split('$pass').join(items);
						items = "";
						for(i=0;i<rows2.length;i++){
							item = falseHTML.split('$headingFail').join("headingFail"+i)
							item = item.split('$collapseFail').join("collapseFail"+i)
							item = item.split('$message').join(rows2[i].exeption)
							item = item.split('$testcaseName').join(rows2[i].name_test)
							item = item.split('$image').join("http://10.0.0.40:2706"+rows2[i].image_path)
							item = item.split('$log').join(rows2[i].stack)
							items = items+item
						}
						html = html.split('$fail').join(items);
						res.send(html)
					});	
				});
		 	});
		}catch(errr){
			console.log(errr);
		}
     	
	})
}