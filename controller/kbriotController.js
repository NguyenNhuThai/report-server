
var mysql = require('mysql');
var bodyParser = require('body-parser');
var http =  require("http");
var fs =  require("fs");
var fs_extra = require('fs-extra')
var urlencodeparser = bodyParser.urlencoded({extended: false})
var zephyr = require('./JWTJar/zephyrintergrate.js')
var sys = require('sys');
var exec = require('child_process').exec;
var sql = require("mssql");

var sql_server_config = {
   user: 'kbrpocdev',
   password: '0EenLYEku6',
   server: 'kbrpocdev.database.windows.net',
   database: 'kbrpoc-qa',
   options: { encrypt: 'true', database: 'kbrpoc-qa'}
};

var db_auto_config = {
  host: "10.0.0.25",
  user: "root",
  password: "root",
  database:"automation_kbr",
  connectionLimit: 15,
  queueLimit: 30,
  acquireTimeout: 1000000
};

initDB = function(port,callback){
    var mongo = require('mongodb'),
        Server = mongo.Server,
        Db = mongo.Db;

    var dbRetry = 420;
    var connect = function(){
        var dbServer = new Server('localhost', parseInt(port), {poolSize :100,auto_reconnect: true,safe:true});
        db = new Db('automationframework', dbServer);
        db.open(function(err, db) {
            if(!err) {
                if (callback) callback();
                console.log("DB connected");
            }
            else{
                if(dbRetry == 0){
                    console.error("Couldn't connect to MongoDB", err.message);
                    process.exit(1);
                }
                dbRetry--;
                setTimeout(connect,1000);
            }
        });
    };
    connect()

};

getDB = function(){
    return db;
};

function prepareText(a){
	a = a.split("'").join("");
	a = a.split('"').join("");
	a = a.split('`').join("");
	return a
}

function handleDisconnect() {
  conection = mysql.createConnection(db_auto_config); 
                                                  
  conection.connect(function(err) {              
    if(err) {                                    
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); 
    }                                    
  });                                     
                                         
  conection.on('error', function(err) {
    console.log('db error', err);
    if(err.code === 'PROTOCOL_CONNECTION_LOST') { 
      handleDisconnect();                        
    } else {                                      
      throw err;                                 
    }
  });
}

handleDisconnect();

module.exports = function(app){
	app.use(bodyParser.json({limit: '50mb'}));
	app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
	app.get("/kbr",function(req,res){
		var fullUrl = req.protocol + '://' + req.get('host');
		res.send("<h2>KBR report module: "+fullUrl+"</h2>" );
	})

	app.get("/kbr/AddeskOccupancy/:deviceCode",function(req,res){
		var queryDetail = `SELECT Floor.BuildingID AS buildingID, Floor.Id AS floorID, Device.Id AS deviceID , Device.ZoneID AS zoneID, Department.Id AS departmentID FROM Floor, Device, Department WHERE Floor.Id = Device.FloorId AND Department.BuildingId = Floor.BuildingId AND Device.Code like '%${req.params.deviceCode}%'`
		sql.connect(sql_server_config).then(() => {
			//if (err) console.log(err);
			var request = new sql.Request();
			request.query(queryDetail, function (err, recordset) {
				if (err) {sql.close();res.send(err);}
				var buildingID = recordset.recordset[0].buildingID;
				var floorID = recordset.recordset[0].floorID;
				var zoneID = recordset.recordset[0].zoneID;
				var deviceID = recordset.recordset[0].deviceID;
				var departmentID = recordset.recordset[0].departmentID;
				var today = new Date();
				var dd = today.getDate();
				var mm = today.getMonth()+1; //January is 0!
				var yyyy = today.getFullYear();
				if(dd<10) {
					dd = '0'+dd
				} 
				if(mm<10) {
					mm = '0'+mm
				} 
				today = mm + '/' + dd + '/' + yyyy;
				var queryAdd = `INSERT INTO dbo.DeskOccupancyReport (Id, AbsentTime, BuildingId, DeparmentId, DeviceId, FloorId, OccupiedTime, ZoneId, TotalDevice) VALUES (NEWID(), '${today}', '${buildingID}', '${departmentID}', '${deviceID}', '${floorID}', '${today}', '${zoneID}', '1');
`
				var request2 = new sql.Request();
				request2.query(queryAdd, function (err2, recordset2) {
					if (err2) {sql.close();res.send(err2);}
					sql.close();
					res.send(recordset2);
				});
			});	
		})
	});

	app.get("/kbr/AddTimeAttendent/:deviceCode",function(req,res){
		var queryGetFloorBuilding = `SELECT BuildingID, ID AS FloorID FROM Floor WHERE Id IN (SELECT FloorId FROM Device WHERE code LIKE '%${req.params.deviceCode}%')`
		sql.connect(sql_server_config).then(() => {
			//if (err) console.log(err);
			var request = new sql.Request();
			request.query(queryGetFloorBuilding, function (err, recordset) {
				if (err) {sql.close();res.send(err);}
				var buildingID = recordset.recordset[0].BuildingID;
				var floorID = recordset.recordset[0].FloorID;
				var today = new Date();
				var dd = today.getDate();
				var mm = today.getMonth()+1; //January is 0!
				var yyyy = today.getFullYear();
				if(dd<10) {
					dd = '0'+dd
				} 
				if(mm<10) {
					mm = '0'+mm
				} 
				today = mm + '/' + dd + '/' + yyyy;
				var queryAddTime = `INSERT INTO dbo.TimeAttendance (Id, DeviceCode, EventDate, EventTime, SensorStatus, BuildingId, FloorId) VALUES (NEWID(), N'${req.params.deviceCode}', '${today}', '06:19:10.3098118', N'In', '${buildingID}', '${floorID}');`
				var request2 = new sql.Request();
				request2.query(queryAddTime, function (err2, recordset2) {
					if (err2) {sql.close();res.send(err2);}
					sql.close();
					res.send(recordset2);
				});
			});	
		})
	});

	app.get("/kbr/AddTimeAttendent/out/:deviceCode",function(req,res){
		var queryGetFloorBuilding = `SELECT BuildingID, ID AS FloorID FROM Floor WHERE Id IN (SELECT FloorId FROM Device WHERE code LIKE '%${req.params.deviceCode}%')`
		sql.connect(sql_server_config).then(() => {
			//if (err) console.log(err);
			var request = new sql.Request();
			request.query(queryGetFloorBuilding, function (err, recordset) {
				if (err) {sql.close();res.send(err);}
				var buildingID = recordset.recordset[0].BuildingID;
				var floorID = recordset.recordset[0].FloorID;
				var today = new Date();
				var dd = today.getDate();
				var mm = today.getMonth()+1; //January is 0!
				var yyyy = today.getFullYear();
				if(dd<10) {
					dd = '0'+dd
				} 
				if(mm<10) {
					mm = '0'+mm
				} 
				today = mm + '/' + dd + '/' + yyyy;
				var queryAddTime = `INSERT INTO dbo.TimeAttendance (Id, DeviceCode, EventDate, EventTime, SensorStatus, BuildingId, FloorId) VALUES (NEWID(), N'${req.params.deviceCode}', '${today}', '06:19:10.3098118', N'Out', '${buildingID}', '${floorID}');`
				var request2 = new sql.Request();
				request2.query(queryAddTime, function (err2, recordset2) {
					if (err2) {sql.close();res.send(err2);}
					sql.close();
					res.send(recordset2);
				});
			});	
		})
	});

	app.get("/kbr/screenshot/:id",function(req,res){
		initDB(27017,function(){
			var db = getDB();
			console.log("id: " +req.params.id)
			db.collection('screenshots', function(err, collection) {
				collection.findOne({_id:req.params.id}, {},function(err,file){
					if(file != null){
						res.contentType("image/png");
						res.end(file.file.buffer, "binary");
					}
					else{
						res.end("Error: file not found.", "utf8");
					}
				})
			}); 
		})
	})

	app.get("/kbr/lastest",function(req,res){
		try{
			selectLastestBuildKeyQuery = "SELECT build_key FROM build_key_history ORDER BY id DESC LIMIT 1";
				conection.query(selectLastestBuildKeyQuery,function(err3,rows3){
					if(err3){
						throw err3;
					}
					var html = fs.readFileSync(__dirname+"/reporttemplate/reportBoostrapTemplateKBR.html","utf8");
					var passHTML = fs.readFileSync(__dirname+"/reporttemplate/passTemplateKBR.html","utf8");
					var falseHTML = fs.readFileSync(__dirname+"/reporttemplate/failTemplate.html","utf8");
					var query = "SELECT * FROM test_result WHERE build_key = '"+ rows3[0].build_key+"' AND result = 'Passed' "
					var query2 = "SELECT * FROM test_result WHERE build_key = '"+ rows3[0].build_key+"' AND result = 'Failed' "
					var date = rows3[0].build_key.substring(0, 2)+"-"+rows3[0].build_key.substring(2, 4)+"-"+rows3[0].build_key.substring(4, 8);
					var time = rows3[0].build_key.substring(8, 10)+":"+rows3[0].build_key.substring(10, 12);
					var item = "";
					conection.query(query,function(err,rows){
						if(err){
							throw err;
						}
						conection.query(query2,function(err2,rows2){
							//res.writeHead(200,{'Conten-Type':'text/html'})
							if(err2){
								throw err2;
							}
							html = html.split('$numberPass').join(rows.length);
							html = html.split('$numberFail').join(rows2.length);
							html = html.split('$numberTestcase').join(rows.length+rows2.length);
							html = html.split('$startTime').join(time);
							html = html.split('$date').join(date);
							var items = "";
							var ret = [];
							for(i=0;i<rows.length;i++){
								item = passHTML.split('$headingPass').join("headingPass"+i)
								item = item.split('$collapsePass').join("collapsePass"+i)
								item = item.split('$testcaseName').join(rows[i].name)
								items = items+item
							}
							html = html.split('$pass').join(items);
							items = "";
							for(i=0;i<rows2.length;i++){
								item = falseHTML.split('$headingFail').join("headingFail"+i)
								item = item.split('$collapseFail').join("collapseFail"+i)
								item = item.split('$message').join(rows2[i].message)
								item = item.split('$testcaseName').join(rows2[i].name)
								item = item.split('$image').join(req.protocol + '://' + req.get('host') +"/kbr/screenshot/"+rows2[i].image_id)
								item = item.split('$log').join(rows2[i].exception)
								items = items+item
							}
							html = html.split('$fail').join(items);
							var optionQuery = "SELECT build_key FROM build_key_history ORDER BY id DESC"
							conection.query(optionQuery,function(err4,rows4){
								console.log(rows4)
								var items = "";
								for(i=0;i<rows4.length;i++){
									items = items+"<option>"+rows4[i].build_key+"</option>"
								}
								console.log(items)
								html = html.split('$options').join(items);
								res.send(html)
							})
						});	
					});
				});
			}catch(errr){
				console.log(errr);
			}
	})

	app.get("/kbr/report/:buildkey",function(req,res){
		try{
				var html = fs.readFileSync(__dirname+"/reporttemplate/reportBoostrapTemplateKBR.html","utf8");
				var passHTML = fs.readFileSync(__dirname+"/reporttemplate/passTemplateKBR.html","utf8");
				var falseHTML = fs.readFileSync(__dirname+"/reporttemplate/failTemplate.html","utf8");
				var query = "SELECT * FROM test_result WHERE build_key = '"+ req.params.buildkey+"' AND result = 'Passed' "
				var query2 = "SELECT * FROM test_result WHERE build_key = '"+ req.params.buildkey+"' AND result = 'Failed' "
				var date = req.params.buildkey.substring(0, 2)+"-"+req.params.buildkey.substring(2, 4)+"-"+req.params.buildkey.substring(4, 8);
				var time = req.params.buildkey.substring(8, 10)+":"+req.params.buildkey.substring(10, 12);
				var item = "";
				conection.query(query,function(err,rows){
					if(err){
						throw err;
					}
					conection.query(query2,function(err2,rows2){
						//res.writeHead(200,{'Conten-Type':'text/html'})
						if(err2){
							throw err2;
						}
						html = html.split('$numberPass').join(rows.length);
						html = html.split('$numberFail').join(rows2.length);
						html = html.split('$numberTestcase').join(rows.length+rows2.length);
						html = html.split('$startTime').join(time);
						html = html.split('$date').join(date);
						var items = "";
						var ret = [];
						for(i=0;i<rows.length;i++){
							item = passHTML.split('$headingPass').join("headingPass"+i)
							item = item.split('$collapsePass').join("collapsePass"+i)
							item = item.split('$testcaseName').join(rows[i].name)
							items = items+item
						}
						html = html.split('$pass').join(items);
						items = "";
						for(i=0;i<rows2.length;i++){
							item = falseHTML.split('$headingFail').join("headingFail"+i)
							item = item.split('$collapseFail').join("collapseFail"+i)
							item = item.split('$message').join(rows2[i].message)
							item = item.split('$testcaseName').join(rows2[i].name)
							item = item.split('$image').join(req.protocol + '://' + req.get('host') +"/kbr/screenshot/"+rows2[i].image_id)
							item = item.split('$log').join(rows2[i].exception)
							items = items+item
						}
						html = html.split('$fail').join(items);
						var optionQuery = "SELECT build_key FROM build_key_history ORDER BY id DESC"
							conection.query(optionQuery,function(err4,rows4){
								console.log(rows4)
								var items = "";
								for(i=0;i<rows4.length;i++){
									items = items+"<option>"+rows4[i].build_key+"</option>"
								}
								console.log(items)
								html = html.split('$options').join(items);
								res.send(html)
							})
					});	
				});				
			}catch(errr){
				console.log(errr);
			}
	})


	app.get("/kbr/regisnewrun/:buildkey",function(req,res){
		
		conection.query(`INSERT INTO build_key_history(build_key) VALUES ("${req.params.buildkey}")`, function(err1, rows1){
    		if(err1){
        		throw err1;
    		}else{
				zephyr.createNewCycle().then(function(cycleID){
					console.log("Import CycleID to mysql: " +cycleID)
					conection.query(`INSERT INTO cycle_history(cycle_id) VALUES ("${cycleID}")`, function(err, rows){
						if(err){
							throw err;
						}else{
							console.log('New cycle id have been saved to mysql')
						}
						res.send(rows);
					})
				}).catch(function(err){
					console.log("Error while create new cycle: " +err)
				})
			}
    		
		})
	})


	app.get("/kbr/test",function(req,res){
		conection.query(`SELECT * FROM test_result WHERE id=1`, function(err, rows){
    		if(err){
        		throw err;
    		}else{
				res.send(rows);
			}
    		
		})
	})
	
	app.get("/kbr/compareCurrent/:buildkey",function(req,res){
		var selectLastestBuildKeyQuery = "SELECT build_key FROM build_key_history ORDER BY id DESC LIMIT 1";
		
			conection.query(selectLastestBuildKeyQuery, function(err, rows2){
				if(err){
					throw err;
				}
				var currentBuildKey =rows2[0].build_key
				console.log(`build key2 `,currentBuildKey)
				console.log(`build key1 `,req.params.buildkey)
				
					var query3 = "SELECT * FROM test_result WHERE build_key = '"+ req.params.buildkey+"' AND result = 'Passed'"
					var query4 = "SELECT * FROM test_result WHERE build_key = '"+ req.params.buildkey+"' AND result = 'Failed'"			
					var query5 = "SELECT * FROM test_result WHERE build_key = '"+ currentBuildKey+"' AND result = 'Passed'"
					var query6 = "SELECT * FROM test_result WHERE build_key = '"+ currentBuildKey+"' AND result = 'Failed'"	
					conection.query(query3, function(err3, tagetPass){
						if(err3){
							throw err3;
						}
						conection.query(query4, function(err4, tagetFalse){
							if(err3){
								throw err3;
							}
							conection.query(query5, function(err5, todayPass){
								if(err5){
									throw err5;
								}
								conection.query(query6, function(err6, todayFalse){
									if(err6){
										throw err6;
									}
									console.log(tagetPass)
									console.log(tagetFalse)
									console.log(todayPass)
									console.log(todayFalse)
									for(i=0;i<todayPass.length;i++){
										for(j=0;j<tagetPass.length;j++){
											if(tagetPass[j].name == todayPass[i].name){
												todayPass[i].name = "";
											}
										}
									}
									for(i=0;i<todayFalse.length;i++){
										for(j=0;j<tagetFalse.length;j++){
											if(tagetFalse[j].name == todayFalse[i].name){
												todayFalse[i].name = "";
											}
										}
									}
									var countPass = 0;
									var countFail = 0
									for(i=0;i<todayPass.length;i++){
										if(todayPass[i].name != ""){
											countPass++;
										}
									}
									for(i=0;i<todayFalse.length;i++){
										if(todayFalse[i].name != ""){
											countFail++;
										}
									}

									var passHTML = fs.readFileSync(__dirname+"/reporttemplate/passTemplateKBR.html","utf8");
									var falseHTML = fs.readFileSync(__dirname+"/reporttemplate/failTemplate.html","utf8");
									var compareHTML = fs.readFileSync(__dirname+"/reporttemplate/compare.html","utf8");	
									compareHTML = compareHTML.split('$numberPass').join(countPass);
									compareHTML = compareHTML.split('$numberFail').join(countFail);
									var items = "";
									for(i=0;i<todayPass.length;i++){
										if(todayPass[i].name != ""){
											item = passHTML.split('$headingPass').join("headingPass"+i+"Com")
											item = item.split('$collapsePass').join("collapsePass"+i+"Com")
											item = item.split('$testcaseName').join(todayPass[i].name)
											item = item.split('#accordionPass').join('#accordionPassCom')
											items = items+item
										}
									}
									compareHTML = compareHTML.split('$passCom').join(items);
									items = "";
									for(i=0;i<todayFalse.length;i++){
										if(todayFalse[i].name != ""){
											item = falseHTML.split('$headingFail').join("headingFail"+i+"Com")
											item = item.split('$collapseFail').join("collapseFail"+i+"Com")
											item = item.split('$message').join(todayFalse[i].message)
											item = item.split('$testcaseName').join(todayFalse[i].name)
											item = item.split('$image').join(req.protocol + '://' + req.get('host') +"/kbr/screenshot/"+todayFalse[i].image_id)
											item = item.split('$log').join(todayFalse[i].exeption)
											item = item.split('#accordionFail').join('#accordionFailCom')
											items = items+item
										}
									}
									compareHTML = compareHTML.split('$failCom').join(items);
									res.send(compareHTML)
								})
							})
						})
					})
			})
	})

	app.get("/kbr/compare/:buildkey/:currentBuildKey",function(req,res){
				var currentBuildKey = req.params.currentBuildkey
				var buildkey = req.params.buildkey
				console.log(`build key2 `,currentBuildKey)
				console.log(`build key1 `,buildkey)
				
					var query3 = "SELECT * FROM test_result WHERE build_key = '"+ buildkey+"' AND result = 'Passed'"
					var query4 = "SELECT * FROM test_result WHERE build_key = '"+ buildkey+"' AND result = 'Failed'"			
					var query5 = "SELECT * FROM test_result WHERE build_key = '"+ currentBuildKey+"' AND result = 'Passed'"
					var query6 = "SELECT * FROM test_result WHERE build_key = '"+ currentBuildKey+"' AND result = 'Failed'"	
					conection.query(query3, function(err3, tagetPass){
						if(err3){
							throw err3;
						}
						conection.query(query4, function(err4, tagetFalse){
							if(err3){
								throw err3;
							}
							conection.query(query5, function(err5, todayPass){
								if(err5){
									throw err5;
								}
								conection.query(query6, function(err6, todayFalse){
									if(err6){
										throw err6;
									}
									console.log(tagetPass)
									console.log(tagetFalse)
									console.log(todayPass)
									console.log(todayFalse)
									for(i=0;i<todayPass.length;i++){
										for(j=0;j<tagetPass.length;j++){
											if(tagetPass[j].name == todayPass[i].name){
												todayPass[i].name = "";
											}
										}
									}
									for(i=0;i<todayFalse.length;i++){
										for(j=0;j<tagetFalse.length;j++){
											if(tagetFalse[j].name == todayFalse[i].name){
												todayFalse[i].name = "";
											}
										}
									}
									var countPass = 0;
									var countFail = 0
									for(i=0;i<todayPass.length;i++){
										if(todayPass[i].name != ""){
											countPass++;
										}
									}
									for(i=0;i<todayFalse.length;i++){
										if(todayFalse[i].name != ""){
											countFail++;
										}
									}

									var passHTML = fs.readFileSync(__dirname+"/reporttemplate/passTemplateKBR.html","utf8");
									var falseHTML = fs.readFileSync(__dirname+"/reporttemplate/failTemplate.html","utf8");
									var compareHTML = fs.readFileSync(__dirname+"/reporttemplate/compare.html","utf8");	
									compareHTML = compareHTML.split('$numberPass').join(countPass);
									compareHTML = compareHTML.split('$numberFail').join(countFail);
									var items = "";
									for(i=0;i<todayPass.length;i++){
										if(todayPass[i].name != ""){
											item = passHTML.split('$headingPass').join("headingPass"+i+"Com")
											item = item.split('$collapsePass').join("collapsePass"+i+"Com")
											item = item.split('$testcaseName').join(todayPass[i].name)
											item = item.split('#accordionPass').join('#accordionPassCom')
											items = items+item
										}
									}
									compareHTML = compareHTML.split('$passCom').join(items);
									items = "";
									for(i=0;i<todayFalse.length;i++){
										if(todayFalse[i].name != ""){
											item = falseHTML.split('$headingFail').join("headingFail"+i+"Com")
											item = item.split('$collapseFail').join("collapseFail"+i+"Com")
											item = item.split('$message').join(todayFalse[i].message)
											item = item.split('$testcaseName').join(todayFalse[i].name)
											item = item.split('$image').join(req.protocol + '://' + req.get('host') +"/kbr/screenshot/"+todayFalse[i].image_id)
											item = item.split('$log').join(todayFalse[i].exeption)
											item = item.split('#accordionFail').join('#accordionFailCom')
											items = items+item
										}
									}
									compareHTML = compareHTML.split('$failCom').join(items);
									res.send(compareHTML)
								})
							})
						})
					})
			
	})


	app.post("/kbr/postResult",urlencodeparser,function(req,res){
		//res.send("<h1>Hello Anh thái</h1>")
		try{
			var a= req.body.des;
			a = a.split("'").join("");
			a = a.split('"').join("");
			a = a.split('`').join("");
			a = a.split('\\').join("");
			var b= req.body.exception;
			b = b.split("'").join("");
			b = b.split('"').join("");
			b = b.split('`').join("");
			b = b.split('`').join("");
			b = b.split('(').join("");
			b = b.split(')').join("");
			b = b.split('{').join("");
			b = b.split('}').join("");
			b = b.split('\\').join("");
			var c = req.body.image_path.replace("core","");
			
			var e = req.body.stack;
			e = e.split("'").join("");
			e = e.split('"').join("");
			e = e.split('`').join("");
			e = e.split('`').join("");
			e = e.split('(').join("");
			e = e.split(')').join("");
			e = e.split('{').join("");
			e = e.split('}').join("");
			e = e.split('\\').join("");
			if(e.length > 10000){
				e = e.substring(1,1000)
			}
			if(b.length > 1000){
				b = b.substring(1,990)
			}
			selectLastestBuildKeyQuery = "SELECT build_key FROM build_key_history ORDER BY id DESC LIMIT 1";
			conection.query(selectLastestBuildKeyQuery,function(err3,rows3){
				if(err3){
					throw err3;
				}
				var buildkey = rows3[0].build_key	
				var query = "INSERT INTO test_result(build_key,testcase_id,name,result,exception,image_id,message) VALUES('"+buildkey  +"','"+ req.body.idtestlink +"','"+ a +"','"+ req.body.status+"','"+ b +"','"+c+"','"+e+"')";
				console.log(query)
				conection.query(query,function(err, rows){
					if(err){
						throw err;
					}
					console.log("Push test case success");
					//res.send(`200-put result to database success`);
					selectLastestCycleQuery = "SELECT cycle_id FROM cycle_history ORDER BY id DESC LIMIT 1";
					conection.query(selectLastestCycleQuery,function(err4,rows4){
						if(err4){
							throw err4;
						}else{
							var cycleID = rows4[0].cycle_id
							console.log("Add execution to cycle: " + cycleID)
							zephyr.updateTestResult(cycleID,a+"--"+req.body.idtestlink,req.body.status,c,e).then(function(text){
								console.log("execution id: ", text, " have been created")
								res.send("INSERT INTO test_result(build_key,testcase_id,name,result,exception,image_id,message) VALUES('"+buildkey  +"','"+ req.body.idtestlink +"','"+ a +"','"+ req.body.status+"','"+ b +"','"+c+"','"+e+"')");
							}).catch(function(e){
								console.log("error: " ,e)
							})
						}
					});
				});
			});
			
			
    		
			
			
		}catch(errr){
			console.log(errr);
		}   
	})
}