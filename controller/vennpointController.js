
var mysql = require('mysql');
var bodyParser = require('body-parser');
var http =  require("http");
var fs =  require("fs");
var fs_extra = require('fs-extra')
var urlencodeparser = bodyParser.urlencoded({extended: false})
var zephyr = require('./JWTJar/zephyrintergrate-vennpoint.js')
var sys = require('sys');
var exec = require('child_process').exec;
var db_auto_config = {
  host: "10.0.0.25",
  user: "root",
  password: "root",
  database:"automation_vennpoint",
  connectionLimit: 15,
  queueLimit: 30,
  acquireTimeout: 1000000
};

function prepareText(a){
	a = a.split("'").join("");
	a = a.split('"').join("");
	a = a.split('`').join("");
	return a
}

function handleDisconnect() {
  con = mysql.createConnection(db_auto_config); 
                                                  
  con.connect(function(err) {              
    if(err) {                                    
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); 
    }                                    
  });                                     
                                         
  con.on('error', function(err) {
    console.log('db error', err);
    if(err.code === 'PROTOCOL_CONNECTION_LOST') { 
      handleDisconnect();                        
    } else {                                      
      throw err;                                 
    }
  });
}
initDB = function(port,callback){
    var mongo = require('mongodb'),
        Server = mongo.Server,
        Db = mongo.Db;

    var dbRetry = 420;
    var connect = function(){
        var dbServer = new Server('localhost', parseInt(port), {poolSize :100,auto_reconnect: true,safe:true});
        db = new Db('automationframework', dbServer);
        db.open(function(err, db) {
            if(!err) {
                if (callback) callback();
                console.log("DB connected");
            }
            else{
                if(dbRetry == 0){
                    console.error("Couldn't connect to MongoDB", err.message);
                    process.exit(1);
                }
                dbRetry--;
                setTimeout(connect,1000);
            }
        });
    };
    connect()

};

getDB = function(){
    return db;
};
handleDisconnect();

module.exports = function(app){
	app.use(bodyParser.json({limit: '50mb'}));
	app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
	app.get("/vennpoint",function(req,res){
		res.send("<h2>Vennpoint report module</h2>");
	})
	

	app.get("/vennpoint/regisnewrun/:buildkey",function(req,res){
		
		con.query(`INSERT INTO build_key_history(build_key) VALUES ("${req.params.buildkey}")`, function(err1, rows1){
    		if(err1){
        		throw err1;
    		}else{
			//	zephyr.createNewCycle().then(function(cycleID){
					console.log("Import CycleID to mysql: " +cycleID)
					con.query(`INSERT INTO cycle_history(cycle_id) VALUES ("${cycleID}")`, function(err, rows){
						if(err){
							throw err;
						}else{
							console.log('New cycle id have been saved to mysql')
						}
						res.send(rows);
					})
			//	}).catch(function(err){
			//		console.log("Error while create new cycle: " +err)
			//	})
			}
    		
		})
	})

app.post("/vennpoint/postResult",urlencodeparser,function(req,res){
		//res.send("<h1>Hello Anh thái</h1>")
		try{
			var a= req.body.des;
			a = a.split("'").join("");
			a = a.split('"').join("");
			a = a.split('`').join("");
			a = a.split('\\').join("");
			var b= req.body.exception;
			b = b.split("'").join("");
			b = b.split('"').join("");
			b = b.split('`').join("");
			b = b.split('`').join("");
			b = b.split('(').join("");
			b = b.split(')').join("");
			b = b.split('{').join("");
			b = b.split('}').join("");
			b = b.split('\\').join("");
			var c = req.body.image_path.replace("core","");
			
			var e = req.body.stack;
			e = e.split("'").join("");
			e = e.split('"').join("");
			e = e.split('`').join("");
			e = e.split('`').join("");
			e = e.split('(').join("");
			e = e.split(')').join("");
			e = e.split('{').join("");
			e = e.split('}').join("");
			e = e.split('\\').join("");
			if(e.length > 10000){
				e = e.substring(1,1000)
			}
			if(b.length > 1000){
				b = b.substring(1,990)
			}
			selectLastestBuildKeyQuery = "SELECT build_key FROM build_key_history ORDER BY id DESC LIMIT 1";
			con.query(selectLastestBuildKeyQuery,function(err3,rows3){
				if(err3){
					throw err3;
				}
				var buildkey = rows3[0].build_key	
				var query = "INSERT INTO test_result(build_key,testcase_id,name,result,exception,image_id,message) VALUES('"+buildkey  +"','"+ req.body.idtestlink +"','"+ a +"','"+ req.body.status+"','"+ b +"','"+c+"','"+e+"')";
				console.log(query)
				con.query(query,function(err, rows){
					if(err){
						throw err;
					}
					console.log("Push test case success");
					//res.send(`200-put result to database success`);
					selectLastestCycleQuery = "SELECT cycle_id FROM cycle_history ORDER BY id DESC LIMIT 1";
					con.query(selectLastestCycleQuery,function(err4,rows4){
						if(err4){
							throw err4;
						}else{
							var cycleID = rows4[0].cycle_id
							console.log("Add execution to cycle: " + cycleID)
						//	zephyr.updateTestResult(cycleID,a+"--"+req.body.idtestlink,req.body.status,c,e).then(function(text){
								console.log("execution id: ", text, " have been created")
								res.send("INSERT INTO test_result(build_key,testcase_id,name,result,exception,image_id,message) VALUES('"+buildkey  +"','"+ req.body.idtestlink +"','"+ a +"','"+ req.body.status+"','"+ b +"','"+c+"','"+e+"')");
						//	}).catch(function(e){
						//		console.log("error: " ,e)
						//	})
						}
					});
				});
			});
			
		}catch(errr){
			console.log(errr);
		}   
	})

		app.get("/vennpoint/lastest",function(req,res){
		try{
			selectLastestBuildKeyQuery = "SELECT build_key FROM build_key_history ORDER BY id DESC LIMIT 1";
				con.query(selectLastestBuildKeyQuery,function(err3,rows3){
					if(err3){
						throw err3;
					}
					var html = fs.readFileSync(__dirname+"/reporttemplate/reportBoostrapTemplateVennpoint.html","utf8");
					var passHTML = fs.readFileSync(__dirname+"/reporttemplate/passTemplateVennpoint.html","utf8");
					var falseHTML = fs.readFileSync(__dirname+"/reporttemplate/failTemplate.html","utf8");
					var query = "SELECT * FROM test_result WHERE build_key = '"+ rows3[0].build_key+"' AND result = 'Passed' "
					var query2 = "SELECT * FROM test_result WHERE build_key = '"+ rows3[0].build_key+"' AND result = 'Failed' "
					var date = rows3[0].build_key.substring(0, 2)+"-"+rows3[0].build_key.substring(2, 4)+"-"+rows3[0].build_key.substring(4, 8);
					var time = rows3[0].build_key.substring(8, 10)+":"+rows3[0].build_key.substring(10, 12);
					var item = "";
					con.query(query,function(err,rows){
						if(err){
							throw err;
						}
						con.query(query2,function(err2,rows2){
							//res.writeHead(200,{'Conten-Type':'text/html'})
							if(err2){
								throw err2;
							}
							html = html.split('$numberPass').join(rows.length);
							html = html.split('$numberFail').join(rows2.length);
							html = html.split('$numberTestcase').join(rows.length+rows2.length);
							html = html.split('$startTime').join(time);
							html = html.split('$date').join(date);
							var items = "";
							var ret = [];
							for(i=0;i<rows.length;i++){
								item = passHTML.split('$headingPass').join("headingPass"+i)
								item = item.split('$collapsePass').join("collapsePass"+i)
								item = item.split('$testcaseName').join(rows[i].name)
								items = items+item
							}
							html = html.split('$pass').join(items);
							items = "";
							for(i=0;i<rows2.length;i++){
								item = falseHTML.split('$headingFail').join("headingFail"+i)
								item = item.split('$collapseFail').join("collapseFail"+i)
								item = item.split('$message').join(rows2[i].message)
								item = item.split('$testcaseName').join(rows2[i].name)
								item = item.split('$image').join(req.protocol + '://' + req.get('host') +"/kbr/screenshot/"+rows2[i].image_id)
								item = item.split('$log').join(rows2[i].exception)
								items = items+item
							}
							html = html.split('$fail').join(items);
							var optionQuery = "SELECT build_key FROM build_key_history ORDER BY id DESC"
							con.query(optionQuery,function(err4,rows4){
								console.log(rows4)
								var items = "";
								for(i=0;i<rows4.length;i++){
									items = items+"<option>"+rows4[i].build_key+"</option>"
								}
								console.log(items)
								html = html.split('$options').join(items);
								res.send(html)
							})
						});	
					});
				});
			}catch(errr){
				console.log(errr);
			}
	})

	app.get("/vennpoint/report/:buildkey",function(req,res){
		try{
				var html = fs.readFileSync(__dirname+"/reporttemplate/reportBoostrapTemplateKBR.html","utf8");
				var passHTML = fs.readFileSync(__dirname+"/reporttemplate/passTemplateKBR.html","utf8");
				var falseHTML = fs.readFileSync(__dirname+"/reporttemplate/failTemplate.html","utf8");
				var query = "SELECT * FROM test_result WHERE build_key = '"+ req.params.buildkey+"' AND result = 'Passed' "
				var query2 = "SELECT * FROM test_result WHERE build_key = '"+ req.params.buildkey+"' AND result = 'Failed' "
				var date = req.params.buildkey.substring(0, 2)+"-"+req.params.buildkey.substring(2, 4)+"-"+req.params.buildkey.substring(4, 8);
				var time = req.params.buildkey.substring(8, 10)+":"+req.params.buildkey.substring(10, 12);
				var item = "";
				con.query(query,function(err,rows){
					if(err){
						throw err;
					}
					con.query(query2,function(err2,rows2){
						//res.writeHead(200,{'Conten-Type':'text/html'})
						if(err2){
							throw err2;
						}
						html = html.split('$numberPass').join(rows.length);
						html = html.split('$numberFail').join(rows2.length);
						html = html.split('$numberTestcase').join(rows.length+rows2.length);
						html = html.split('$startTime').join(time);
						html = html.split('$date').join(date);
						var items = "";
						var ret = [];
						for(i=0;i<rows.length;i++){
							item = passHTML.split('$headingPass').join("headingPass"+i)
							item = item.split('$collapsePass').join("collapsePass"+i)
							item = item.split('$testcaseName').join(rows[i].name)
							items = items+item
						}
						html = html.split('$pass').join(items);
						items = "";
						for(i=0;i<rows2.length;i++){
							item = falseHTML.split('$headingFail').join("headingFail"+i)
							item = item.split('$collapseFail').join("collapseFail"+i)
							item = item.split('$message').join(rows2[i].message)
							item = item.split('$testcaseName').join(rows2[i].name)
							item = item.split('$image').join(req.protocol + '://' + req.get('host') +"/vennpoint/screenshot/"+rows2[i].image_id)
							item = item.split('$log').join(rows2[i].exception)
							items = items+item
						}
						html = html.split('$fail').join(items);
						var optionQuery = "SELECT build_key FROM build_key_history ORDER BY id DESC"
							con.query(optionQuery,function(err4,rows4){
								console.log(rows4)
								var items = "";
								for(i=0;i<rows4.length;i++){
									items = items+"<option>"+rows4[i].build_key+"</option>"
								}
								console.log(items)
								html = html.split('$options').join(items);
								res.send(html)
							})
					});	
				});				
			}catch(errr){
				console.log(errr);
			}
	})
	app.get("/vennpoint/screenshot/:id",function(req,res){
		initDB(27017,function(){
			var db = getDB();
			console.log("id: " +req.params.id)
			db.collection('screenshots', function(err, collection) {
				collection.findOne({_id:req.params.id}, {},function(err,file){
					if(file != null){
						res.contentType("image/png");
						res.end(file.file.buffer, "binary");
					}
					else{
						res.end("Error: file not found.", "utf8");
					}
				})
			}); 
		})
	})

/*
	app.post("/vennpoint/regisnewrun",urlencodeparser,function(req,res){
		try{
			var query1 = "SELECT * FROM RunTimeControl WHERE SuiteName = '" +req.body.SuiteName + "' ORDER BY RunTime DESC LIMIT 1";
			console.log(query1)
			con.query(query1,function(err,rows){
			 	if(err){
        			throw err;
    			}
				if(rows.length == 0){
					var query = "INSERT INTO RunTimeControl(SuiteName,RunTime,Time,IP) VALUES ('"+req.body.SuiteName+"','1','"+req.body.Time+"','"+req.body.IP+"')";
					console.log(query)
					con.query(query,function(err1){
						if(err1){
							throw err1;
						}
					});							
				}else{
					var query = "INSERT INTO RunTimeControl(SuiteName,RunTime,Time,IP) VALUES ('"+req.body.SuiteName+"','"+ (parseInt(rows[0].RunTime)+1) +"','"+req.body.Time+"','"+req.body.IP+"')";
					console.log(query)
					con.query(query,function(err1){
						if(err1){
							throw err1;
						}
					});			
				}
		 	});
			res.send("HTTP/1.1 200 OK");
		}catch(errr){
			console.log(errr);
		}   
	})
*/
	app.get("/vennpoint/getruntime/:suitename",function(req,res){
		con.query(`SELECT * FROM RunTimeControl WHERE SuiteName = '${req.params.suitename}' ORDER BY RunTime DESC LIMIT 1`, function(err, rows){
    		if(err){
        		throw err;
    		}
    		res.send(rows);
		})
	})
/*
	app.post("/vennpoint/postresuts",urlencodeparser,function(req,res){
		try{
			var runtime = req.body.RunTime;
			runtime = prepareText(runtime);
			runtime = parseInt(runtime);
			var testlinkid = req.body.TestLinkId;
			testlinkid = prepareText(testlinkid);
			var name =  req.body.Name;
			name = prepareText(name);
			var message =  req.body.Message;
			message = prepareText(message);
			var result =  req.body.Result;
			result = prepareText(result);
			var exception =  req.body.Exception;
			exception = prepareText(exception);
			var image =  req.body.imageBase64;
			image = prepareText(image);
			var suitename = req.body.SuiteName;
			suitename = prepareText(suitename);
			var query = `INSERT INTO test_result(RunTime,TestLinkId,Name,Result,Message,Exception,imageBase64,SuiteName) VALUES('${runtime}','${testlinkid}','${name}','${result}','${message}','${exception}','${image}','${suitename}')`
			
			con.query(query, function(err, rows){
    			if(err){
        			throw err;
    			}
    			res.send("HTTP/1.1 200 OK");
			})

		}catch(err){
			console.log(err)
		}
	})

	app.get("/vennpoint/lastestreport", function(req,res){
		
		try{

			selectLastestBrowserQuery = "SELECT * FROM RunTimeControl WHERE SuiteName = 'test-web-app' ORDER BY RunTime DESC LIMIT 1";
			selectLastestAndroidQuery = "SELECT * FROM RunTimeControl WHERE SuiteName = 'test-android' ORDER BY RunTime DESC LIMIT 1";
			selectLastestIosQuery = "SELECT * FROM RunTimeControl WHERE SuiteName = 'test-ios' ORDER BY RunTime DESC LIMIT 1";

			con.query(selectLastestBrowserQuery,function(err1,rows1){
			 	if(err1){
        			throw err1;
    			}
				con.query(selectLastestAndroidQuery,function(err2,rows2){
					if(err2){
						throw err2;
					}
					con.query(selectLastestIosQuery,function(err3,rows3){
						if(err3){
							throw err3;
						}
						var html = fs.readFileSync(__dirname+"/reporttemplate/vennpointReportTemplate.html","utf8");
						var passHTML = fs.readFileSync(__dirname+"/reporttemplate/passTemplate.html","utf8");
						var falseHTML = fs.readFileSync(__dirname+"/reporttemplate/failTemplate.html","utf8");

						if(rows1.length != 0){
							var query1 = "SELECT * FROM test_result WHERE RunTime = '"+ rows1[0].RunTime+"' AND result = 'passed' AND TestLinkId <> '' AND SuiteName = '"+rows1[0].SuiteName+"'"
							var query2 = "SELECT * FROM test_result WHERE RunTime = '"+ rows1[0].RunTime+"' AND result = 'failed' AND TestLinkId <> '' AND SuiteName = '"+rows1[0].SuiteName+"'"
							var dateBrowser = rows1[0].Time.substring(0, 2)+"-"+rows1[0].Time.substring(2, 4)+"-"+rows1[0].Time.substring(4, 8);
							var timeBrowser = rows1[0].Time.substring(8, 10)+":"+rows1[0].Time.substring(10, 12);
							
							con.query(query1,function(err4,rows4){
								if(err4){
									throw err4;
								}
								con.query(query2,function(err5,rows5){
									//res.writeHead(200,{'Conten-Type':'text/html'})
									if(err5){
										throw err5;
									}
									html = html.split('$pcNumberPass').join(rows4.length);
									html = html.split('$pcNumberFail').join(rows5.length);
									html = html.split('$pcNumberTestcase').join(rows4.length+rows5.length);
									html = html.split('$pcStartTime').join(timeBrowser);
									html = html.split('$pcDate').join(dateBrowser);
									var items = "";
									var ret = [];
									for(i=0;i<rows4.length;i++){
										item = passHTML;
										item = item.split('$headingPass').join("headingPass"+i)
										item = item.split('$collapsePass').join("collapsePass"+i)
										item = item.split('$testcaseName').join(rows4[i].Name)
										item = item.split('$image').join("data:image/png;base64,"+rows4[i].ImageBase64)
										items = items+item
									}
									html = html.split('$pcPass').join(items);
									items = "";
									for(i=0;i<rows5.length;i++){
										item = falseHTML
										item = item.split('$headingFail').join("headingFail"+i)
										item = item.split('$collapseFail').join("collapseFail"+i)
										item = item.split('$message').join(rows5[i].Message)
										item = item.split('$testcaseName').join(rows5[i].Name)
										item = item.split('$image').join("data:image/png;base64,"+rows5[i].ImageBase64)
										item = item.split('$log').join(rows5[i].Exception)
										items = items+item
									}
									html = html.split('$pcFail').join(items);

									if(rows2.length != 0){
										var query3 = "SELECT * FROM test_result WHERE RunTime = '"+ rows2[0].RunTime+"' AND result = 'passed' AND TestLinkId <> '' AND SuiteName = '"+rows2[0].SuiteName+"'"
										var query4 = "SELECT * FROM test_result WHERE RunTime = '"+ rows2[0].RunTime+"' AND result = 'failed' AND TestLinkId <> '' AND SuiteName = '"+rows2[0].SuiteName+"'"
										var dateAndroid = rows2[0].Time.substring(0, 2)+"-"+rows2[0].Time.substring(2, 4)+"-"+rows2[0].Time.substring(4, 8);
										var timeAndroid = rows2[0].Time.substring(8, 10)+":"+rows2[0].Time.substring(10, 12);
										con.query(query3,function(err6,rows6){
											if(err6){
												throw err6;
											}
											con.query(query4,function(err7,rows7){
												if(err7){
													throw err7;
												}
												html = html.split('$anNumberPass').join(rows6.length);
												html = html.split('$anNumberFail').join(rows7.length);
												html = html.split('$anNumberTestcase').join(rows6.length+rows7.length);
												html = html.split('$anStartTime').join(timeAndroid);
												html = html.split('$anDate').join(dateAndroid);
												var items = "";
												var ret = [];
												for(i=0;i<rows6.length;i++){
													item = passHTML;
													item = item.split('$headingPass').join("headingPass"+i+"an")
													item = item.split('$collapsePass').join("collapsePass"+i+"an")
													item = item.split('$testcaseName').join(rows6[i].Name)
													item = item.split('$image').join("data:image/png;base64,"+rows6[i].ImageBase64)
													items = items+item
												}
												html = html.split('$anPass').join(items);
												items = "";
												for(i=0;i<rows7.length;i++){
													item = falseHTML
													item = item.split('$headingFail').join("headingFail"+i+"an")
													item = item.split('$collapseFail').join("collapseFail"+i+"an")
													item = item.split('$message').join(rows7[i].Message)
													item = item.split('$testcaseName').join(rows7[i].Name)
													item = item.split('$image').join("data:image/png;base64,"+rows7[i].ImageBase64)
													item = item.split('$log').join(rows7[i].Exception)
													items = items+item
												}
												html = html.split('$anFail').join(items);
												if(rows3.length != 0){
													var query5 = "SELECT * FROM test_result WHERE RunTime = '"+ rows3[0].RunTime+"' AND result = 'passed' AND TestLinkId <> '' AND SuiteName = '"+rows3[0].SuiteName+"'"
													var query6 = "SELECT * FROM test_result WHERE RunTime = '"+ rows3[0].RunTime+"' AND result = 'failed' AND TestLinkId <> '' AND SuiteName = '"+rows3[0].SuiteName+"'"
													var dateIOS = rows3[0].Time.substring(0, 2)+"-"+rows3[0].Time.substring(2, 4)+"-"+rows3[0].Time.substring(4, 8);
													var timeIOS = rows3[0].Time.substring(8, 10)+":"+rows3[0].Time.substring(10, 12);
													con.query(query5,function(err8,rows8){
														if(err8){
															throw err8;
														}
														con.query(query6,function(err9,rows9){
															if(err9){
																throw err9;
															}
															html = html.split('$iosNumberPass').join(rows8.length);
															html = html.split('$iosNumberFail').join(rows9.length);
															html = html.split('$iosNumberTestcase').join(rows8.length+rows9.length);
															html = html.split('$iosStartTime').join(timeIOS);
															html = html.split('$iosDate').join(dateIOS);
															var items = "";
															var ret = [];
															for(i=0;i<rows8.length;i++){
																item = passHTML;
																item = item.split('$headingPass').join("headingPass"+i+"ios")
																item = item.split('$collapsePass').join("collapsePass"+i+"ios")
																item = item.split('$testcaseName').join(rows8[i].Name)
																item = item.split('$image').join("data:image/png;base64,"+rows8[i].ImageBase64)
																items = items+item
															}
															html = html.split('$iosPass').join(items);
															items = "";
															for(i=0;i<rows9.length;i++){
																item = falseHTML
																item = item.split('$headingFail').join("headingFail"+i+"ios")
																item = item.split('$collapseFail').join("collapseFail"+i+"ios")
																item = item.split('$message').join(rows9[i].Message)
																item = item.split('$testcaseName').join(rows9[i].Name)
																item = item.split('$image').join("data:image/png;base64,"+rows9[i].ImageBase64)
																item = item.split('$log').join(rows9[i].Exception)
																items = items+item
															}
															html = html.split('$iosFail').join(items);
															res.send(html);
														});	
													});
												}else{
													res.send(html);
												}
											});	
										});
									}else{
										res.send(html);
									}
								});	
							});
						}
					});
				});
			});
		}catch(errr){
			console.log(errr);
		}
	})
	*/
}